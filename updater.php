<?php

class ENHR_Updater {

    private $file;

    private $plugin;

    private $basename;

    private $active;

    private $repository;

    // private $authorize_token;

    private $gitlab_response;

    public function __construct( $file, $configuration) {

        $this->file = $file;

        add_action( 'admin_init', array( $this, 'set_plugin_properties' ) );

        $this->repository = $configuration['repository_id'];
        $this->gitlab_response = $this->get_repository_info();

        return $this;
    }

    public function set_plugin_properties() {
        $this->plugin	= get_plugin_data( $this->file );
        $this->basename = plugin_basename( $this->file );
        $this->active	= is_plugin_active( $this->basename );
    }

    private function get_repository_info()
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://gitlab.com/api/v4/projects/'.$this->repository.'/releases',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "PRIVATE-TOKEN: 7awmixigiyezyZnwHZ_h"
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        $release = (json_decode($response))[0];

        return $release;
    }

    public function initialize() {
        add_filter( 'site_transient_update_plugins', array( $this, 'check_update' ), 10, 3 );
        add_filter( 'plugins_api', array( $this, 'plugin_popup' ), 10, 3);
        add_filter( 'upgrader_post_install', array( $this, 'after_install' ), 10, 3 );
    }

    private function downloadFile()
    {
        // check if file exists
        $download_file = wp_get_upload_dir()['basedir'].'/enhrpublisher/'.$this->gitlab_response->tag_name.'.zip';
        if ( ! file_exists($download_file))
        {
            file_put_contents($download_file, file_get_contents($this->gitlab_response->assets->sources[0]->url));
        }

        return $download_file;
    }

    public function check_update( $transient )
    {
        if ( property_exists( $transient, 'checked') ) { // Check if transient has a checked property

            if ( $checked = $transient->checked ) { // Did Wordpress check for updates?

                if (empty($checked[$this->basename]))
                    return;

                $out_of_date = version_compare( $this->gitlab_response->tag_name, $checked[ $this->basename ], 'gt' );

                if ($out_of_date) {
                    $this->downloadFile();

                    $new_files = wp_get_upload_dir()['basedir'] . '/enhrpublisher/' . $this->gitlab_response->tag_name.'.zip';

                    $slug = current( explode('/', $this->basename ) );

                    $plugin = array(
                        // setup our plugin info
                        'url' => $this->plugin["PluginURI"],
                        'slug' => $slug,
                        'package' => $new_files,
                        'new_version' => $this->gitlab_response->tag_name,
                    );

                    $transient->response[$this->basename] = (object) $plugin; // Return it in response
                }
            }
        }

        return $transient;
    }

    public function plugin_popup($result, $action, $args)
    {
        if ( ! empty($args->slug ))
        {
            if ($args->slug == current(explode( '/' , $this->basename)) )
            {
                // Set it to an array
                $plugin = [
                    'name'				        => $this->plugin["Name"],
                    'slug'				        => $this->basename,
                    'requires'					=> '3.3',
                    'tested'					=> '5.3.2',
                    'rating'					=> '100.0',
                    'num_ratings'				=> '1',
                    'added'						=> '2020-02-15',
                    'version'			=> $this->gitlab_response->tag_name,
                    'author'			=> $this->plugin["AuthorName"],
                    'author_profile'	=> $this->plugin["AuthorURI"],
                    // 'last_updated'		=> $this->github_response['published_at'],
                    'homepage'			=> $this->plugin["PluginURI"],
                    'short_description' => $this->plugin["Description"],
                    'sections'			=> [
                        'Description'	=> $this->plugin["Description"],
                        // 'Updates'	=> $this->github_response['body'],
                    ],
                    'download_link'		=> wp_get_upload_dir()['basedir'] . '/enhrpublisher/' . $this->gitlab_response->tag_name.'.zip',
                    'new_version'           => $this->gitlab_response->tag_name,
                ];

                return (object) $plugin; // Return the data
            }

        }

        return $result; // Otherwise return default
    }

    /* private function removeOldFiles()
    {
        return true;
    } */

    public function after_install( $response, $hook_extra, $result ) {
        global $wp_filesystem;

        $install_directory = plugin_dir_path( $this->file );
        $wp_filesystem->move( $result['destination'], $install_directory );
        $result['destination'] = $install_directory;

        // $this->removeOldFiles();

        if ( $this->active )
        {
            activate_plugin( $this->basename );
        }

        return $result;
    }
}

