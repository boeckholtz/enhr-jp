# ENHR-JP

Description

* Modular Administration Manager
* CPT Manager
* ACF Integration
* Logging System
* Multilingual support

## Install instructions
[Download ENHR-JP plugin](https://gitlab.com/)
1. After downloading extract folder in wp-content/plugins
2. Check if form fields need adjustments for `enhr-apply.js` to work as expected.

In the folder example you'll find a example of a fully functional apply form.

Example:

`<input class="required" type="text" name="apply[firstName]" data-error="Vul je voornaam in">`

The attribute data-error is used when a field is empty or didn't validate.
Example: 

apply[emailAddres], apply[phoneNumber] and apply[file] have an extra validation.

```
  let $email    = $('#apply-form [type=email]'+requiredFieldName);
  let $phone    = $('#apply-form [type=number]'+requiredFieldName);
  let $file     = $('#apply-form [type=file]'+requiredFieldName);
  let $gender   = $('#apply-form [type=radio]'+requiredFieldName);
  let $button   = $('#apply-form [type="submit"]');
```

If you want to use the full power of validation and not the default browser validation of your browser use `class="required"` and set the `requiredFieldName` in `enhr-jp.js` to `.required`.

### reCAPTCHA
If you have set the recaptcha value in the ENHR settings you only need to add the following code in your form:

`echo \enhr\Helpers\Recaptcha::loadCaptcha();`

Note that when you have not set a captcha key in ENHR settings, it obviously won't show a recaptcha.