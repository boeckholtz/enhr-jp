<?php
/**
 * @package: enhr-jp
 */

namespace enhr\Pages;


use enhr\Api\SettingsApi;
use enhr\Base\BaseController;
use enhr\Api\Callbacks\AdminCallbacks;

class Applying extends BaseController {

	public
		$settings,
		$callbacks,
		$subpages = [];

	public function __invoke() {

		$this->settings = new SettingsApi();
		$this->callbacks = new AdminCallbacks();
		$this->setSubPages();
		$this->settings->addSubPages( $this->subpages )();
	}

	public function setSubPages() {

		$this->subpages = [
			[
				'parent_slug' => __SLG__,
				'page_title'  => 'Applying',
				'menu_title'  => 'Statistieken',
				'capability'  => 'manage_options',
				'menu_slug'   => __SLG__ . '_applying',
				'callback'    => [ $this->callbacks, 'adminApplying' ],
			]
		];
	}
}
