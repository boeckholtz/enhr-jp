<?php
/**
 * @package: enhr-jp
 */

namespace enhr\Pages;

use enhr\Api\Callbacks\ManagerCallbacks;
use enhr\Base\BaseController;
use enhr\Api\SettingsApi;
use enhr\Api\Callbacks\AdminCallbacks;

class Dashboard extends BaseController {

	public
		$settings,
		$callbacks,
		$callbacks_mngr,
		$pages = [];

	public function __invoke() {

		$this->settings = new SettingsApi();

		$this->callbacks      = new AdminCallbacks();
		$this->callbacks_mngr = new ManagerCallbacks();

		$this->setPages();

		$this->setSettings();
		$this->setSections();
		$this->setFields();

		$this->settings->AddPages( $this->pages )->withSubPage( 'General' )();
	}


	public function setPages() {

		$this->pages = [
			[
				'page_title' => strtoupper( __PLGNM__ ),
				'menu_title' => strtoupper( __PLGNM__ ),
				'capability' => 'manage_options',
				'menu_slug'  => __SLG__,
				'callback'   => [ $this->callbacks, 'adminGeneral' ],
				'icon_url'   => 'data:image/png;base64,' . base64_encode( file_get_contents( $this->plugin_path . 'assets/enpublisher_logo_no_text.png' ) ),
				'position'   => 110
			]
		];
	}

	public function setSettings() {

		$args = array(
			array(
				'option_group' => __PRFX__ . '_general_settings',
				'option_name'  => __PRFX__ . '_default_language_code',
				'callback'     => 'enhr\Helpers\Filter::languageCode'
			),
			array(
				'option_group' => __PRFX__ . '_general_settings',
				'option_name'  => __PRFX__ . '_default_language_required',
				'callback'     => 'enhr\Helpers\Filter::checkboxSanitize'
			),
			array(
				'option_group' => __PRFX__ . '_general_settings',
				'option_name'  => __PRFX__ . '_send_apply_mails',
				'callback'     => 'enhr\Helpers\Filter::checkboxSanitize'
			),
			array(
				'option_group' => __PRFX__ . '_general_settings',
				'option_name'  => __PRFX__ . '_cron_interval',
				'callback'     => 'enhr\Helpers\Filter::integer'
			),
			array(
				'option_group' => __PRFX__ . '_general_settings',
				'option_name'  => __PRFX__ . '_cron_portion',
				'callback'     => 'enhr\Helpers\Filter::tinyInt'
			),
			array(
				'option_group' => __PRFX__ . '_general_settings',
				'option_name'  => __PRFX__ . '_obsolete_data_removal_period',
				'callback'     => 'enhr\Helpers\Filter::tinyInt'
			),
			array(
				'option_group' => __PRFX__ . '_general_settings',
				'option_name'  => __PRFX__ . '_rest_api_url',
				'callback'     => 'enhr\Helpers\Filter::url'
			),
			array(
				'option_group' => __PRFX__ . '_general_settings',
				'option_name'  => __PRFX__ . '_rest_api_request_token_url',
				'callback'     => 'enhr\Helpers\Filter::url'
			),
			array(
				'option_group' => __PRFX__ . '_general_settings',
				'option_name'  => __PRFX__ . '_rest_api_client_id',
				'callback'     => 'enhr\Helpers\Filter::tinyInt'
			),
			array(
				'option_group' => __PRFX__ . '_general_settings',
				'option_name'  => __PRFX__ . '_rest_api_client_secret_id'
			),
			array(
				'option_group' => __PRFX__ . '_general_settings',
				'option_name'  => __PRFX__ . '_apply_attachments_amount',
				'callback'     => 'enhr\Helpers\Filter::tinyInt'
			),
            array(
                'option_group' => __PRFX__ . '_general_settings',
                'option_name'  => __PRFX__ . '_apply_captcha_client'
            ),
            array(
                'option_group' => __PRFX__ . '_general_settings',
                'option_name'  => __PRFX__ . '_apply_captcha_server'
            ),

            array(
                'option_group'  => __PRFX__ . '_general_settings',
                'option_name'   => __PRFX__ . '_vacancy_slug',
                // 'callback'      => 'ehrh\Helpers\Filter::string',
            ),
		);

		$this->settings->setSettings( $args );
	}

	public function setSections() {

		$args = [
			[
				'id'       => __PRFX__ . '_manage_publisher',
				'title'    => false,
				'callback' => [ $this->callbacks_mngr, 'publisherSectionManager' ],
				'page'     => __SLG__
			],
			[
				'id'    => __PRFX__ . '_admin_general',
				'title' => '<div class="section-title">' . __('Algemene instellingen', self::TEXTDOMAIN) .'</div>',
				'page'  => __SLG__
			],
			[
				'id'    => __PRFX__ . '_admin_api',
				'title' => '<div class="section-title">' . __('API instellingen', self::TEXTDOMAIN) .' <ul class="quick-menu"><li id="locker" state="" class="unlock-icon" title="Unlock/Lock the section fields"></li></ul></div>',
				'page'  => __SLG__
			],
            [
                'id'    => __PRFX__ . '_apply_captcha',
                'title' => '<div class="section-title">' . __('ReCaptcha instellingen', self::TEXTDOMAIN) .'</div>',
                'page'  => __SLG__
            ],
            [
                'id'    => __PRFX__ . '_vacancy_config',
                'title' => '<div class="section-title">' . __('Vacature instellingen', self::TEXTDOMAIN) .'</div>',
                'page'  => __SLG__
            ],
			[
				'id'    => __PRFX__ . '_apply_form',
				'title' => '<div class="section-title">' . __('Sollicitatieformulier instellingen', self::TEXTDOMAIN) .'</div>',
				'page'  => __SLG__
			],
		];

		$this->settings->setSections( $args );
	}

	public function setFields() {

		$args = array(
			array(
				'id'       => __PRFX__ . '_default_language_code',
				'title'    => __('Selecteer Standaard Taal', self::TEXTDOMAIN),
				'callback' => [ $this->callbacks_mngr, 'langCodeSelector' ],
				'page'     => __SLG__,
				'section'  => __PRFX__ . '_admin_general',
				'args'     => array(
					'label_for' => __PRFX__ . '_default_language_code',
					'class'     => 'dr--w100'
				)
			),
			array(
				'title'    => __('Cron job gebruiker', self::TEXTDOMAIN),
				'callback' => [ $this->callbacks_mngr, 'cron_user' ],
				'page'     => __SLG__,
				'section'  => __PRFX__ . '_admin_general',
				'args'     => array(
					'class' => 'dr--w100'
				)
			),
			array(
				'title'    => __('Verstuur e-mails via plugin?', self::TEXTDOMAIN),
				'callback' => [ $this->callbacks_mngr, 'inputCheckbox' ],
				'page'     => __SLG__,
				'section'  => __PRFX__ . '_admin_general',
				'args'     => array(
					'label_for' => __PRFX__ . '_send_apply_mails',
					'class'     => 'dr--w100'
				)
			),
			array(
				'id'       => __PRFX__ . '_cron_interval',
				'title'    => __('Selecteer cron interval (seconden)', self::TEXTDOMAIN),
				'callback' => [ $this->callbacks_mngr, 'inputField' ],
				'page'     => __SLG__,
				'section'  => __PRFX__ . '_admin_general',
				'args'     => array(
					'label_for' => __PRFX__ . '_cron_interval',
					'type'      => 'number',
					'class'     => 'dr--w100'
				)
			),
			array(
				'id'       => __PRFX__ . '_cron_portion',
				'title'    => __('Aantal posts per cron fetch', self::TEXTDOMAIN),
				'callback' => [ $this->callbacks_mngr, 'inputField' ],
				'page'     => __SLG__,
				'section'  => __PRFX__ . '_admin_general',
				'args'     => array(
					'label_for' => __PRFX__ . '_cron_portion',
					'type'      => 'number',
					'class'     => 'dr--w100',
					'attr'      => [
						'min'  => 5,
						'max'  => 250,
						'step' => 5
					]
				)
			),
			array(
				'id'       => __PRFX__ . '_obsolete_data_removal_period',
				'title'    => __('Verwijder overtollige / verouderde gegevens uit de database ouder dan (in dagen)', self::TEXTDOMAIN),
				'callback' => [ $this->callbacks_mngr, 'inputField' ],
				'page'     => __SLG__,
				'section'  => __PRFX__ . '_admin_general',
				'args'     => array(
					'label_for' => __PRFX__ . '_obsolete_data_removal_period',
					'type'      => 'number',
					'class'     => 'dr--w100',
					'attr'      => [
						'min'  => 1,
						'step' => 1
					]
				)
			),
			array(
				'id'       => __PRFX__ . '_rest_api_url',
				'title'    => 'Base URL',
				'callback' => [ $this->callbacks_mngr, 'inputField' ],
				'page'     => __SLG__,
				'section'  => __PRFX__ . '_admin_api',
				'args'     => array(
					'label_for' => __PRFX__ . '_rest_api_url',
					'class'     => 'dr--w100 api-data',
					'attr'      => [
						'readonly' => 'readonly'
					]
				)
			),
			array(
				'id'       => __PRFX__ . '_rest_api_request_token_url',
				'title'    => 'Request token URL',
				'callback' => [ $this->callbacks_mngr, 'inputField' ],
				'page'     => __SLG__,
				'section'  => __PRFX__ . '_admin_api',
				'args'     => array(
					'label_for' => __PRFX__ . '_rest_api_request_token_url',
					'class'     => 'dr--w100 api-data',
					'attr'      => [
						'readonly' => 'readonly'
					]
				)
			),
			array(
				'id'       => __PRFX__ . '_rest_api_client_id',
				'title'    => 'Client ID',
				'callback' => [ $this->callbacks_mngr, 'inputField' ],
				'page'     => __SLG__,
				'section'  => __PRFX__ . '_admin_api',
				'args'     => array(
					'label_for' => __PRFX__ . '_rest_api_client_id',
					'type'      => 'number',
					'class'     => 'dr--w100 api-data',
					'attr'      => [
						'readonly' => 'readonly'
					]
				)
			),

			array(
				'id'       => __PRFX__ . '_rest_api_client_secret_id',
				'title'    => 'Client secret ID',
				'callback' => [ $this->callbacks_mngr, 'inputField' ],
				'page'     => __SLG__,
				'section'  => __PRFX__ . '_admin_api',
				'args'     => array(
					'label_for' => __PRFX__ . '_rest_api_client_secret_id',
					'class'     => 'dr--w100 api-data',
					'attr'      => [
						'readonly' => 'readonly'
					]
				)
			),

            array(
                'id'       => __PRFX__ . '_apply_captcha_client',
                'title'    => 'Client side key',
                'callback' => [ $this->callbacks_mngr, 'inputField' ],
                'page'     => __SLG__,
                'section'  => __PRFX__ . '_apply_captcha',
                'args'     => array(
                    'label_for' => __PRFX__ . '_apply_captcha_client',
                    'class'     => 'dr--w100 api-data',
                    'attr'      => [
                        // 'readonly' => 'readonly'
                    ]
                )
            ),

            array(
                'id' => __PRFX__.'_apply_captcha_server',
                'title'    => __('Server side key', self::TEXTDOMAIN),
                'callback' => [ $this->callbacks_mngr, 'inputField' ],
                'page'     => __SLG__,
                'section'  => __PRFX__ . '_apply_captcha',
                'args'     => array(
                    'label_for' => __PRFX__ . '_apply_captcha_server',
                    'class'     => 'dr--w100'
                )
            ),

            array(
                'id' => __PRFX__.'_vacancy_slug',
                'title'    => __('Vacature url prefix', self::TEXTDOMAIN),
                'callback' => [ $this->callbacks_mngr, 'inputField' ],
                'page'     => __SLG__,
                'section'  => __PRFX__ . '_vacancy_config',
                'args'     => array(
                    'label_for' => __PRFX__ . '_vacancy_slug',
                    'class'     => 'dr--w100',
                )
            ),

            // _vacancy_config

			array(
				'id'       => __PRFX__ . '_apply_attachments_amount',
				'title'    => __('Aantal extra bijlagen', self::TEXTDOMAIN),
				'callback' => [ $this->callbacks_mngr, 'inputField' ],
				'page'     => __SLG__,
				'section'  => __PRFX__ . '_apply_form',
				'args'     => array(
					'label_for' => __PRFX__ . '_apply_attachments_amount',
					'type'      => 'number',
					'class'     => 'dr--w100',
					'attr'      => [
						'min' => 0,
						'max' => 5
					]
				)
			)
		);

		$this->settings->setFields( $args );
	}
}
