<?php
/**
 * @package: enhr-jp
 */

namespace enhr\Base;


class CustomTaxonomyController {

	private $taxonomies = [];

	public function __invoke() {

		add_action( 'init', [ $this, 'registerCustomTaxonomies' ] );

		$this->taxonomies = [
			'country'              => [ 'company', 'vacancy' ],
			'branch_primary'       => [ 'company' ],
			'branch_secondary'     => [ 'company' ],
			'location'             => [ 'vacancy' ],
			'department_primary'   => [ 'vacancy' ],
			'department_secondary' => [ 'vacancy' ],
			'education_primary'    => [ 'vacancy' ],
			'education_secondary'  => [ 'vacancy' ],
			'education_tertiary'   => [ 'vacancy' ]
		];
	}

	public function registerCustomTaxonomies() { 

		$labels = [
			'country'              => [ 'Country', 'Countries' ],
			'location'             => [ 'Location', 'Locations' ],
			'branch_primary'       => [ 'Primary Branch', 'Primary Branches' ],
			'branch_secondary'     => [ 'Secondary Branch', 'Secondary Branches' ],
			'department_primary'   => [ 'Primary Department', 'Primary Departments' ],
			'department_secondary' => [ 'Secondary Department', 'Secondary Departments' ],
			'education_primary'    => [ 'Primary Education', 'Primary Educations' ],
			'education_secondary'  => [ 'Secondary Education', 'Secondary Educations' ],
			'education_tertiary'   => [ 'Thertiary Education', 'Tertiary Educations' ]
		];

		foreach ( $this->taxonomies as $tax => $post_types ) {

			$args = [
				'labels'       => [
					'name'              => $labels[ $tax ][1],
					'singular_name'     => $labels[ $tax ][0],
					'search_items'      => 'Search ' . $labels[ $tax ][1],
					'all_items'         => 'All ' . $labels[ $tax ][1],
					'view_item '        => 'View ' . $labels[ $tax ][0],
					'parent_item'       => 'Parent ' . $labels[ $tax ][0],
					'parent_item_colon' => 'Parent ' . $labels[ $tax ][0] . ':',
					'edit_item'         => 'Edit ' . $labels[ $tax ][0],
					'update_item'       => 'Update ' . $labels[ $tax ][0],
					'add_new_item'      => 'Add New ' . $labels[ $tax ][0],
					'new_item_name'     => 'New ' . $labels[ $tax ][0] . ' Name',
					'menu_name'         => $labels[ $tax ][0],
				],
				'public'       => true,
				'show_in_rest' => false,
			];

			register_taxonomy( $tax, $post_types, $args );
		}
	}
}
