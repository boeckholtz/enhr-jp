<?php
/**
 * @package: enhr-jp
 */

namespace enhr\Base;

class Deactivate {

	public static function deactivate() {
		wp_clear_scheduled_hook('___fetch_job_event');
		flush_rewrite_rules();
	}
}
