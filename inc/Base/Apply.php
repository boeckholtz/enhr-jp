<?php
/**
 * @package: enhr-jp
 */

namespace enhr\Base;

use enhr\Helpers\Curly;
use enhr\Helpers\Log;

class Apply extends Curly {

	private
		$lid,
		$status = 'init',
		$upload_path,
		$tmp_name,
		$type,
		$actualType,
		$fileName,
		$fileSize,
		$filePath,
		$attachments,
		$companyData = [],
		$vacancyData = [],
		$emailMessage = '',
		$enhrpublisherApplyFields = [
		'initials'             => [ 'string', 12 ],
		'homeStreet'           => [ 'string', 60 ],
		'homePostalCode'       => [ 'string', 30 ],
		'homeNumberSuffix'     => [ 'string', 10 ],
		'homeNumber'           => [ 'string', 10 ],
		'birthDate'            => [ 'string', 50 ],
		'title'                => [ 'string', 32 ],
		'suffix'               => [ 'string', 32 ],
		'lastNamePrefix'       => [ 'string', 40 ],
		'lastName'             => [ 'alpha', 60 ],
		'firstName'            => [ 'alpha', 40 ],
		'mobileNumberBusiness' => [ 'string', 40 ],
		'mobileNumber'         => [ 'string', 40 ],
		'phoneNumber'          => [ 'string', 40 ],
		'phoneNumberBusiness'  => [ 'string', 40 ],
		'emailAddressBusiness' => [ 'email', null ],
		'emailAddress'         => [ 'email', null ],
		'city'                 => [ 'string', 40 ],
		'fullAddress'          => [ 'string', 40 ],
		'motivation'           => [ 'string', null ],
        'gender'               => [ 'string', '40'],
        'crExpertise_8416'     => [ 'array', null],
    ];

	public function __invoke() {

		$this->log['scope'] = 'apply';

		if ( $_SERVER["REQUEST_METHOD"] === 'POST' AND ! empty( $_POST["apply"] ) ) {

			if ( ! empty( $_POST['g-recaptcha-response'] ) ) {
				\enhr\Helpers\Kit::getCaptcha( 0, $_POST['g-recaptcha-response'] );
			}

			$output          = [ 'data' => $_POST ];
			$output['files'] = $_FILES;

			$tbl_fields = $this->get_db_field_names( __APPLY_TBL__ );

			foreach ( $_POST["apply"] as $key => $values ) {

				if ( ! in_array( $key, $tbl_fields ) ) {
					$this->log['notice'] = 'The field `' . $key . '` is not in the allowed list (removed)';
					unset( $_POST["apply"][ $key ] );
				} else {
					switch ( $this->enhrpublisherApplyFields[ $key ][0] ) {

						case 'string':
							$_POST["apply"][ $key ] = filter_var( trim( $_POST["apply"][ $key ] ), FILTER_SANITIZE_STRING );
							break;
						case 'alpha':
							$_POST["apply"][ $key ] = preg_replace( '/[^[:alpha:]]/u', '', $_POST["apply"][ $key ] );
							break;
						case 'email':

							$_POST["apply"][ $key ] = filter_var( trim( $_POST["apply"]['emailAddress'] ), FILTER_VALIDATE_EMAIL );

							if ( empty( $_POST["apply"][ $key ] ) ) {
								$this->log['exit']['result']  = 'error';
								$this->log['exit']['message'] = __( "Mandatory field `emailAddress` format doesn't meet required format", self::TEXTDOMAIN );
								$this->outputOnExit();
							}
							break;
						case 'array':
						    $_POST["apply"][ $key ] = json_encode($_POST["apply"][ $key ]);
							break;
					}

				}

				if ( $this->enhrpublisherApplyFields[ $key ][1] !== null ) {
					$_POST["apply"][ $key ] = mb_substr( $_POST["apply"][ $key ], 0, $this->enhrpublisherApplyFields[ $key ][1] );
				}
			}

			$_POST["apply"]['ip_address'] = $_SERVER['REMOTE_ADDR'];

			$this->wpdb->insert(
				__APPLY_TBL__,
				$_POST["apply"]
			);

			$this->lid = $this->wpdb->insert_id;
			
		    foreach ( $_POST["apply"] as $key => $values ) {
		        $_POST["apply"][ $key ] = strval($_POST["apply"][ $key ]);
		        if(null !== json_decode($_POST["apply"][ $key ])){
    			    $_POST["apply"][ $key ] = json_decode($_POST["apply"][ $key ]);
		        }
		    }

			/**
			 * Validating vacancy_root_id
			 */
			if ( empty ( $_POST["apply"]['vacancy_root_id'] ) ) {
				$this->log['exit']['result']  = 'error';
				$this->log['exit']['message'] = __( 'Mandatory field `vacancy_root_id` is empty', self::TEXTDOMAIN );
				$this->outputOnExit();
			}

			/**
			 * Validating the eMail address
			 */
			if ( empty( trim( $_POST["apply"]['emailAddress'] ) ) ) {
				$this->log['exit']['result']  = 'error';
				$this->log['exit']['message'] = __( 'Mandatory field `emailAddress` is empty', self::TEXTDOMAIN );

				$this->outputOnExit();
			}


			if ( ! empty( $this->wpdb->error ) ) {
				$this->log['exit']['result']  = 'error';
				$this->log['exit']['message'] = __( 'Could not insert Initial data to the `' . __APPLY_TBL__ . '`. Error: ', self::TEXTDOMAIN ) . $this->wpdb->error;
				$this->outputOnExit();
			}

			$this->log['applied_data'] = $_POST["apply"];

			$this->upload_path = $this->getCachedPath();


			/**
			 * Working with attachments
			 */

			$_POST["apply"]['attachments_quantity'] = $_POST["apply"]['attachments_total_size'] = 0;

			if ( ! empty( $_FILES["apply"]["name"]["CV"] ) ) {

				$this->fileName = $this->touchFile( $_FILES["apply"]["name"]["CV"], 'CV' );

				if ( $_FILES["apply"]["error"]["CV"] == UPLOAD_ERR_OK ) {

					$this->type       = $_FILES["apply"]["type"]["CV"];
					$this->tmp_name   = $_FILES["apply"]["tmp_name"]["CV"];
					$this->actualType = mime_content_type( $this->tmp_name );
					$this->fileSize   = $_FILES["apply"]["size"]["CV"];

					$this->moveUploadedFile( 'CV' );

				} else {

					if ( ! empty( $_FILES["apply"]["name"]["CV"] ) ) {
						$this->log['upload']['cv']['error'][ $this->fileName ] = __( "Couldn't transferred", self::TEXTDOMAIN );
					}
				}
			}

			foreach ( $_FILES["apply"]["error"]["attachments"] as $idx => $error ) {

				$this->fileName = $this->touchFile( $_FILES["apply"]["name"]["attachments"][ $idx ], 'attachment' );

				if ( $error == UPLOAD_ERR_OK ) {

					$this->type       = $_FILES["apply"]["type"]["attachments"][ $idx ];
					$this->tmp_name   = $_FILES["apply"]["tmp_name"]["attachments"][ $idx ];
					$this->actualType = mime_content_type( $this->tmp_name );
					$this->fileSize   = $_FILES["apply"]["size"]["attachments"][ $idx ];

					$this->moveUploadedFile( 'attachment' );

				} else {

					if ( ! empty( $_FILES["apply"]["name"]["attachments"][ $idx ] ) ) {
						$this->log['upload']['attachments']['error'][ $this->fileName ] = __( "Couldn't transferred", self::TEXTDOMAIN );
					}
				}
			}

			if ( $this->wpdb->update(
				__APPLY_TBL__,
				[
					'log_json'               => json_encode( $this->log, JSON_UNESCAPED_UNICODE ),
					'attachments_quantity'   => $_POST["apply"]['attachments_quantity'],
					'attachments_total_size' => $_POST["apply"]['attachments_total_size']
				],
				[ 'id' => $this->lid ]
			) ) {
				$this->status = 'incomplete';
				$this->attachments = json_decode( $this->wpdb->get_var( "SELECT log_json FROM " . __APPLY_TBL__ . " WHERE id = " . $this->lid ), true )["attachments"];
			}

            //determine if TAS should send emails and add key/value to the API request
            $_POST["apply"]["send_ats_email"] = get_option(__PRFX__.'_send_apply_mails') == '1' ? false : true;
            $_POST["apply"]["secret"] = get_option(__PRFX__.'_rest_api_client_secret_id');

			$this->endPoint          = 'applies';
			$this->apply_post_fields = json_encode( $_POST["apply"], JSON_PRETTY_PRINT );
			$setApply                = $this->setApply(); // Helpers\Curly::class
			$apply                   = json_decode( $setApply["content"] );

			$this->log['enpub_response'] = $setApply["content"];

			if ( isset( $apply->result ) AND $apply->result === 'success' ) {

				if ( $this->wpdb->update(
					__APPLY_TBL__,
					[
						'enpub_id' => $apply->apply_id
					],
					[ 'id' => $this->lid ]
				) ) {

					$this->status = 'incomplete';
					foreach ( $this->attachments as $i => $attachment ) {

						if ( file_exists( $attachment['path'] ) ) {

							$apply_post_fields               = [ 'name' => $attachment['name'] ];
							$apply_post_fields['attachment'] = base64_encode( file_get_contents( $attachment['path'] ) );
							$apply_post_fields['size']       = $attachment['size'];
							$apply_post_fields['apply']      = $apply->apply_id;

							$this->endPoint          = 'apply-attachment';
							$this->apply_post_fields = json_encode( $apply_post_fields, JSON_PRETTY_PRINT );

							$setApplyFinal = $this->setApply();
							$applyFinal    = json_decode( $setApplyFinal["content"] );

							$this->log['enpub_response_attachments'] = $setApplyFinal["content"];

							if ( isset( $applyFinal->result ) AND $applyFinal->result === 'success' ) {

								$this->log['attachments'][$i]['enpub_response'] = $applyFinal->result;
								$this->log['attachments'][$i]['unlink'] = unlink($attachment['path']);

								$this->wpdb->update(
									__APPLY_TBL__,
									[
										'log_json' => json_encode( $this->log, JSON_UNESCAPED_UNICODE )
									],
									[ 'id' => $this->lid ]
								);

								$this->status = 'publish';

							} else {

								$this->status = 'incomplete';
								$this->log['attachments'][$i]['enpub_response'] = $applyFinal->result;
								$this->log['exit']['result']  = 'incomplete';
								$this->log['exit']['message'] = __("Could not publish the attached file: ", self::TEXTDOMAIN ) . $attachment['name'];
								$this->outputOnExit();
							}
						}
					}
				}

				$this->getEmailData();

				if ( wp_doing_ajax() ) {
					require_once( ABSPATH . '/wp-includes/pluggable.php' );
				}

				include_once WP_PLUGIN_DIR . '/enhr-jp/templates/emailMessage.php';

				/**
				 * Finally sending email message to the applicant
				 */
				
				if(get_option(__PRFX__.'_send_apply_mails') == '1'){
				    $this->log['exit']['emailSent'] = wp_mail( trim( $_POST["apply"]['emailAddress'] ), $this->vacancyData['title'], $this->emailMessage, [
					    'From: '.get_option('blogname').' <'.get_option('admin_email').'>',
					    'Content-Type: text/html; charset=UTF-8'
				    ] );
				}

                $this->wpdb->delete(
                    __APPLY_TBL__,
                    [ 'id' => $this->lid ]
                );

				$this->log['exit']['result']  = 'success';
				$this->log['exit']['message'] = __( 'The data have been processed successfully', self::TEXTDOMAIN );

			} else {
				$this->log['exit']['result']  = 'error';
				$this->log['exit']['message'] = $this->log['curl_exit_msg'];
			}

			$this->outputOnExit();
		}
	}

	private function getEmailData(): void {

		$vacancy_post_id            = $this->wpdb->get_var( "SELECT post_id FROM {$this->wpdb->prefix}postmeta where meta_key = '" . __PRFX__ . "_vacancy_root_id' AND meta_value = '{$_POST["apply"]['vacancy_root_id']}' LIMIT 1;" );

		$this->vacancyData['title'] = html_entity_decode( get_the_title( $vacancy_post_id ) );
		$this->vacancyData['meta']  = get_post_meta( $vacancy_post_id );

		$this->companyData['company_post_id'] = $this->wpdb->get_var( "SELECT post_id FROM {$this->wpdb->prefix}postmeta where meta_key = '" . __PRFX__ . "_company_root_id' AND meta_value = '{$this->vacancyData['meta'][ __PRFX__ .'_vacancy_company_root_id'][0]}' LIMIT 1;" );
		$this->companyData['company_title']   = html_entity_decode( get_the_title( $this->companyData['company_post_id'] ) );
		$this->companyData['meta']            = get_post_meta( $this->companyData['company_post_id'] );
		$this->companyData["thumbnail_url"]   = get_the_post_thumbnail_url( $this->companyData['company_post_id'], 'full' );
		$this->companyData["thumbnail_id"]    = get_post_thumbnail_id( $this->companyData['company_post_id'] );
		$this->companyData["thumbnail_path"]  = get_attached_file( $this->companyData["thumbnail_id"], 'full' );

		$this->log['applied_data']['vacancy_post_id']    = $vacancy_post_id;
		$this->log['applied_data']['vacancy_post_title'] = $this->vacancyData['title'];
		$this->log['applied_data']['company_post_id']    = $this->companyData['company_post_id'];
		$this->log['applied_data']['company_post_title'] = $this->companyData['company_title'];
	}

	private function moveUploadedFile( $type ) {
		# 8MB
		if ( $this->fileSize < 8000000 AND $this->upload_path !== false ) {
			$this->fileName = basename( $this->fileName );
			$this->filePath = $this->upload_path . $this->fileName;
			if ( move_uploaded_file( $this->tmp_name, $this->filePath ) ) {
				$this->log['attachments'][ $_POST["apply"]['attachments_quantity'] ]['name'] = $this->fileName;
				$this->log['attachments'][ $_POST["apply"]['attachments_quantity'] ]['path'] = $this->filePath;
				$this->log['attachments'][ $_POST["apply"]['attachments_quantity'] ]['size'] = $this->fileSize;
				$this->log['attachments'][ $_POST["apply"]['attachments_quantity'] ]['type'] = $this->type;

				$_POST["apply"]['attachments_quantity'] ++;
				$_POST["apply"]['attachments_total_size'] += $this->fileSize;
			}
		} else {
			$this->log['upload'][ $type ]['error'][ $this->fileName ] = __( "Couldn't be uploaded, size is ", self::TEXTDOMAIN ) . round( $this->fileSize / 1048576, 2 ) . " MB";
		}
	}


	public function getCachedPath() {

		$cachedPath = __DIR_JP_APPLIES_ATTACHMENTS__ . __PRFX__ . '_files' . DIRECTORY_SEPARATOR;

		if ( ! file_exists( $cachedPath ) ) {
			return ( mkdir( $cachedPath, 0777, true ) ) ? $cachedPath : false;
		} else {
			return $cachedPath;
		}
	}

	/**
	 * @param $tbl_name
	 *
	 * @return array
	 *
	 * Getting names of the field from the table
	 */
	private function get_db_field_names( $tbl_name ): array {
		$field_names = [];

		foreach ( $this->wpdb->get_results( "SHOW COLUMNS FROM " . $tbl_name, 'ARRAY_A' ) as $value ) {
			$field_names[] = $value["Field"];
		}

		return $field_names;
	}


	private function outputOnExit() {


		$this->wpdb->update(
			__APPLY_TBL__,
			[
				'log_json' => json_encode( $this->log, JSON_UNESCAPED_UNICODE ),
				'status'   => $this->status
			],
			[ 'id' => $this->lid ]
		);

		$this->log['exit']['scope'] = $this->log['scope'];

		Log::doJsonOutput( $this->log['exit'] );
		wp_die();
	}

	private function touchFile( $filename, $type = '_' ){

		return str_pad($this->lid, 6, '0', STR_PAD_LEFT  ) . '_' . $type . '_' .   $filename;
	}

	public static function renderRows( $top ){

		$rows = '';

		foreach ( $top as $vacancy ) {

			$frequent = $vacancy->most_frequent;

			$log = json_decode( $vacancy->log, true)['applied_data'];

			if ( ! empty( $log['vacancy_post_title'] ) ) {

				if( $vacancy_link = get_permalink( $log['vacancy_post_id'] ) ){
					$vacancy = "<a href=\"" . $vacancy_link . "\" target='_blank' >{$log['vacancy_post_title']}</a>";
				}else{
					$vacancy = "<span>{$log['vacancy_post_title']}</span>";
				}

				$rows .= "<div class='a-row'>
        <span style='display: inline-block; float: right'>{$frequent}</span>{$vacancy} by 
        <a href=\"" . get_permalink( $log['company_post_id'] ) . "\" target='_blank' >{$log['company_post_title']}</a>
        </div>" . PHP_EOL;

			}
		}

		return $rows;
	}
}
