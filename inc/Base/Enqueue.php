<?php
/**
 * @package: enhr-jp
 */

namespace enhr\Base;

use \enhr\Base\BaseController;

class Enqueue extends BaseController {

	public function __invoke() {
		add_action( 'admin_enqueue_scripts', [ $this, 'admin_enqueue_scripts' ] );
	}

	public function admin_enqueue_scripts( $hook ) {

		wp_enqueue_style( __PRFX__ . '-admin-style', $this->plugin_url . 'assets/' . __PRFX__ . '-admin.css', false, $this->plugin_version );
		wp_enqueue_script( __PRFX__ . '-admin-js', $this->plugin_url . 'assets/' . __PRFX__ . '-admin.js', false, $this->plugin_version, true );

		if ( $hook == 'post.php' || $hook == 'post-new.php' ) {
			wp_enqueue_script( __PRFX__ . '-admin-cpt-js', $this->plugin_url . 'assets/' . __PRFX__ . '-admin-cpt.js', false, $this->plugin_version, true );
		}
	}

}