<?php
/**
 * @package: enhr-jp
 */

namespace enhr\Base;

class CustomPostTypeController extends BaseController {

	/**
	 * @href https://shibashake.com/wordpress-theme/custom-post-type-permalinks-part-2
	 * @href https://wordpress.stackexchange.com/questions/222797/remove-parent-slugs-from-urls-on-three-customs-posts-types/222802
	 * @href https://php-academy.kiev.ua/blog/the-rewrite-api-post-types-taxonomies
	 */
	public function __invoke() {
		add_action( 'init', [ $this, 'registerCustomPostTypes' ] );
		//add_action( 'generate_rewrite_rules', [ $this, 'generate_rewrite_rules' ] );
		//add_filter( 'post_type_link', [ $this, 'vacancy_link' ], 10, 2 );

	}

	public function registerCustomPostTypes() {

		register_post_type( 'company', array(
				'labels'             => array(
					'name'          => __( 'Companies', self::TEXTDOMAIN ),
					'singular_name' => __( 'Company', self::TEXTDOMAIN ),
					'add_new'       => __( 'Add new', self::TEXTDOMAIN ),
					'add_new_item'  => __( 'Add new company', self::TEXTDOMAIN ),
					'not_found'     => __( 'No companies found', self::TEXTDOMAIN ),
				),
				'public'             => true,
				'publicly_queryable' => true,
				'query_var'          => true,
				'has_archive'        => false,
				'show_in_rest'       => false,
				'menu_position'      => 2,
				'menu_icon'          => 'dashicons-universal-access',
				'supports'           => [ 'title', 'thumbnail', 'excerpt' ],
				'rewrite'            => true,
				'hierarchical'       => false
			)
		);

		// Check for vacancy Slug
        $vacancy_option_slug    = get_option(__PRFX__.'_vacancy_slug');
        $vacancy_slug           = (empty($vacancy_option_slug)) ? 'vacacture' : $vacancy_option_slug;

		register_post_type( 'vacancy', array(
				'labels'             => array(
					'name'          => __( 'Vacancies', self::TEXTDOMAIN ),
					'singular_name' => __( 'Vacancy', self::TEXTDOMAIN ),
					'add_new'       => __( 'Add new', self::TEXTDOMAIN ),
					'add_new_item'  => __( 'Add new vacancy', self::TEXTDOMAIN ),
					'not_found'     => __( 'No vacancies found', self::TEXTDOMAIN ),
				),
				'public'             => true,
				'publicly_queryable' => true,
				'query_var'          => true,
				'has_archive'        => true,
				'show_ui'            => true,
				'show_in_rest'       => false,
				'menu_position'      => 2,
				'supports'           => [ 'title', 'thumbnail', 'excerpt' ],
				'taxonomies'         => [ 'post_tag' ],
				'rewrite'            => [ 'slug' => __($vacancy_slug.'/%company%', self::TEXTDOMAIN), 'with_front' => true ],
				'hierarchical'       => false
			)
		);

		//flush_rewrite_rules();
	}


	public function generate_rewrite_rules( $wp_rewrite ) {

		$rules = [];

		foreach ( [ 'company', 'vacancy' ] as $cpt ) {

			$post_type = get_post_types( array(
				'name'     => $cpt,
				'public'   => true,
				'_builtin' => false
			), 'objects' );

			switch ( $post_type->name ) {
				case 'company':
					$company_slug = $post_type->rewrite['slug'];
					break;
				case 'vacancy':
					$vacancy_slug = $post_type->rewrite['slug'];
					break;
			}

			$rules[ $company_slug . '/' . $vacancy_slug . '/?$' ] = 'vacancy=' . $vacancy_slug;

		}
		$wp_rewrite->rules = $rules + $wp_rewrite->rules;
	}

	public function vacancy_link( $permalink, $id = 0 ) {

		$post = get_post( $id );

		if ( $post->post_type != 'vacancy' ) {
			return $permalink;
		}

		$query = new \WP_Query( [
			'posts_per_page' => 1,
			'post_type'      => 'company',
			'meta_query'     => [
				[
					'key'   => __PRFX__ . '_company_root_id',
					'value' => get_post_meta( $post->ID, __PRFX__ . '_vacancy_company_root_id', true ),
					'type'  => 'INT'
				]
			],
			'fields'         => 'ids'
		] );


		if ( $slug = get_post( $query->posts[0] )->post_name ) {
			$permalink = str_replace( 'vacancy', $slug, $permalink );
		}

		//\enhr\Helpers\Log::dmp( $post_link, true );

		return $permalink;
	}
}