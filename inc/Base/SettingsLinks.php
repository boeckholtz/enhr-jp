<?php
/**
 * Created by PhpStorm.
 * User: theAmok
 * Date: 05.12.2018
 * Time: 16:41
 */

namespace enhr\Base;

use \enhr\Base\BaseController;

class SettingsLinks extends BaseController {

	public function __invoke() {
		add_filter( "plugin_action_links_$this->plugin", [ $this, 'settings_link' ] );
	}

	public function settings_link( $links ) {

		$settings_link = '<a href="options-general.php?page=enhr_jp">Instellingen</a>';
		array_push( $links, $settings_link );

		return $links;
	}
}
