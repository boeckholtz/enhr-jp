<?php
/**
 * @package: enhr-jp
 */

namespace enhr\Base;


trait Helper {

	public function sanitizeInteger( $int, $min = 0, $max = DAY_IN_SECONDS, $default = 600 ) {

		return filter_var( $int, FILTER_VALIDATE_INT, [
				'options' => [
					'default'   => $default,
					'min_range' => $min,
					'max_range' => $max
				]
			]
		);
	}
}