<?php
/**
 * @package: enhr-jp
 */

namespace enhr\Base;

use enhr\Helpers\Filter;


class Activate {

	use Filter;

	public static function activate() {

		if (!file_exists(__DIR_JP_MEDIA_UPLOAD__)) {
			mkdir( __DIR_JP_MEDIA_UPLOAD__ );
		}

		add_option( __PRFX__ . '_cron_interval', 600, '', 'yes' );
		add_option( __PRFX__ . '_send_apply_mails', 0, '', 'yes' );
		add_option( __PRFX__ . '_cron_lock', 0, '', 'no' );
		add_option( __PRFX__ . '_active_user', get_users( [ 'role__in' => [ 'administrator' ] ] )[0]->data->ID );
		add_option( __PRFX__ . '_default_language_code', self::languageCode( '' ) );
		add_option( __PRFX__ . '_default_language_required', 1 );
		add_option( __PRFX__ . '_cron_portion', 25 );
		add_option( __PRFX__ . '_apply_attachments_amount', 1 );
		add_option( __PRFX__ . '_obsolete_data_removal_period', 14, '','yes' );

		wp_clear_scheduled_hook( '___fetch_job_event' );
		wp_schedule_event( time(), '___fetch_job', '___fetch_job_event' );
		flush_rewrite_rules();

		require_once ABSPATH . 'wp-admin/includes/upgrade.php';

		$sql = "CREATE TABLE " . __LOG_TBL__ . " (
                  id INT UNSIGNED NOT NULL AUTO_INCREMENT,
                  job ENUM('default', 'cron', 'manual') NOT NULL DEFAULT 'default',
                  scope ENUM('default','companies','vacancies','application') NOT NULL DEFAULT 'default',                
                  request_time_float DECIMAL(14,4) UNSIGNED NULL,
                  end_time DECIMAL(14,4) UNSIGNED NULL,
                  affected INT UNSIGNED NULL,
                  description TEXT,
                  log_raw TEXT,
                  logged_gmt timestamp NULL,
                  PRIMARY KEY (id));";

		dbDelta( $sql );

		$sql = "CREATE TABLE " . __APPLY_TBL__ . " (
				id BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
				 enpub_id BIGINT(20) UNSIGNED DEFAULT NULL,
				 logged TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
				 ip_address VARCHAR(45) DEFAULT NULL,
				 log_json LONGTEXT,
				 status ENUM('init','incomplete','publish','complete','error','republish') NOT NULL DEFAULT 'init',
				 attachments_quantity TINYINT(3) UNSIGNED DEFAULT NULL,
				 attachments_total_size INT(11) UNSIGNED DEFAULT NULL,
				 vacancy_root_id VARCHAR(32) DEFAULT NULL,
				 title VARCHAR(255) NOT NULL,
				 firstName VARCHAR(127) DEFAULT NULL,
				 lastName VARCHAR(127) DEFAULT NULL,
				 emailAddress VARCHAR(255) DEFAULT NULL,
				 phoneNumber VARCHAR(45) DEFAULT NULL,
				 motivation TEXT,
				 lastNamePrefix TEXT,
			 	 gender TEXT,
				 crExpertise_8416 TEXT,
				PRIMARY KEY (id) );";
		dbDelta( $sql );
	}
}
