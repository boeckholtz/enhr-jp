<?php
/**
 * @package: enhr-jp
 */

namespace enhr\Base;

use enhr\Helpers\MultiLingual;
use enhr\Helpers\Log;
use enhr\Helpers\Time;

class BaseController {

	use Time, MultiLingual, Log;

	public const TEXTDOMAIN = __PRFX__;

	public static $media_upload_folder, $wp_nonce, $user = false;

	public
		$admin,
		$active_user,
		$wpdb,
		$log = [],
		$salt = '2e3b5786',
		$api_access_token,
		$api_base_url,
		$plugin,
		$plugin_path,
		$templates,
		$managers = [],
		$plugin_url,
		$plugin_version,
		$languages,
		$isMultiLingual,
		$primaryLanguage,
		$query_language_codes;

	public function __construct() {

		global $wpdb;

		$this->active_user          = get_option( __PRFX__ . '_active_user' );
		$this->wpdb                 = $wpdb;
		$this->api_base_url         = get_option( __PRFX__ . '_rest_api_url' );
		$this->api_access_token     = json_decode( get_option( __PRFX__ . '_access_token' ), true );
		$this->plugin               = plugin_basename( dirname( __FILE__, 3 ) ) . '/' . __PLGNM__ . '.php';
		$this->plugin_path          = plugin_dir_path( dirname( __FILE__, 2 ) );
		$this->templates            = $this->plugin_path . 'templates/';
		$this->plugin_url           = plugin_dir_url( dirname( __FILE__, 2 ) );
		$this->plugin_version       = time();
		$this->languages            = self::getLanguages();
		$this->isMultiLingual       = is_array( $this->languages ) ? true : false;
		$this->primaryLanguage      = $this->isMultiLingual ? $this->languages['default_language_code'] : $this->languages;
		$this->query_language_codes = $this->isMultiLingual ? $this->languages['query_language_codes'] : $this->languages;

		add_action( 'init', [ $this, 'setAdminUser' ] );

		add_action( 'init', function () {
			self::$user = wp_get_current_user();
		} );
	}

	/**
	 * Getting data and setting as a current  user for the plugin.
	 * Note: the first admin from the users.
	 */
	public function setAdminUser() {

		$this->admin = get_userdata( $this->active_user );

		if ( current_user_can( 'delete_others_posts' ) === false ) {

			wp_set_current_user( $this->admin->ID );
		}
	}

	public function getCompanyData( $vacancy_root_id ): array {

		require_once ABSPATH . DIRECTORY_SEPARATOR . WPINC . '/post-thumbnail-template.php';

		$data = [];

		$args = array(
			'posts_per_page' => 1,
			'post_type'      => 'company',
			'meta_query'     => [
				[
					'key'   => __PRFX__ . '_company_root_id',
					'value' => get_post_meta( $vacancy_root_id, __PRFX__ . '_vacancy_company_root_id', true ),
					'type'  => 'INT'
				]
			]
		);

		$query = new \WP_Query( $args );

		$data['post'] = $query->posts[0];
		$data['meta'] = get_post_meta( $data['post']->ID, '', true );


		return $data;
	}
}