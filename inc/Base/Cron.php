<?php
/**
 * @package: enhr-jp
 */

namespace enhr\Base;


use enhr\Api\Publisher\Company;
use enhr\Api\Publisher\Job;
use enhr\Api\Publisher\Vacancy;
use enhr\Helpers\Filter;

class Cron {

	use Filter;

	public function __invoke() {

		add_action( 'updated_option', [ $this, 'reschedule___fetch_job' ], 10, 3 );
		add_filter( 'cron_schedules', [ $this, 'fetch_job' ] );
		add_action( '___fetch_job_event', [ $this, 'doSync' ] );

	}

	public function doSync( $args = [] ) {

		\enhr\Helpers\Log::dmp( 'Job started @ ' . date( 'Y-m-d H:i:s' ), true );

		$scopes = [ "Company", "Vacancy" ];

		if ( self::lock() ) {

			$cronLog = ['message' => 'error'];
			$cronLog['message_description'] = __('Cron Job is running... execution is prohibited. Try in a few minutes after ', BaseController::TEXTDOMAIN ) . date( 'Y-m-d H:i:s', self::getLockDelay() );

			return $cronLog;
		}

		if ( ! empty( $args['scope'] ) ) {
			$scopes = explode( '|', $args['scope'] );
		}

		$resp = [];
		$job  = new Job();

		foreach ( $scopes as $scope ) {

			if ( $scope == 'Company' ) {
				$resp['Company'] = $job->init( new Company(), $args );
			}

			if ( $scope == 'Vacancy' ) {
				$resp['Vacancy'] = $job->init( new Vacancy(), $args );
			}
		}

		/**
		 * Republish the applies from the web-form which could not be performed during submission
		 */
		$deferred = new \enhr\Api\Publisher\Deferred;
		$deferred->republishApplies();

		/**
		 * Remove redundant / obsolete data from the `_enhr_applies` table older than X days
		 */
		$deferred->removeObsoleteData( 'applies' );

		self::unlock();
		//$resp['Cache'] = 'not flushed'; 
		if (function_exists('w3tc_flush_all')){
            w3tc_flush_all();
            //$resp['Cache'] = 'flushed';
        }

		return $resp;
	}

	private static function lock() {

		/*update_option( __PRFX__ . '_cron_lock', 0 );*/ //temp

		$lt = time();

		$lock_delay = self::getLockDelay();

		if ( $lock_delay < $lt ) {
			update_option( __PRFX__ . '_cron_lock', $lt );

			return false;
		} else {
			return true;
		}
	}

	private static function getLockDelay() {
		return ( ( (int) get_option( __PRFX__ . '_cron_interval' ) + 180 ) + (int) get_option( __PRFX__ . '_cron_lock' ) );
	}

	public static function unlock() {
		update_option( __PRFX__ . '_cron_lock', 0 );
	}

	/**
	 * Adding a new cron scheduled job
	 *
	 * @param $schedules
	 *
	 * @return mixed
	 */
	public function fetch_job( $schedules ) {

		$schedules['___fetch_job'] = array(
			'interval' => Filter::integer( get_option( __PRFX__ . '_cron_interval' ) ),
			'display'  => 'Synchronise with EnhrPublisher'
		);

		return $schedules;
	}

	/**
	 * Change the interval of cron job if option changed via setting page of the plugin
	 *
	 * @param $option
	 * @param $old_value
	 * @param $value
	 */
	function reschedule___fetch_job( $option, $old_value, $value ) {

		if ( $option !== __PRFX__ . '_cron_interval' ) {
			return;
		}

		$value = Filter::integer( $value );

		/**
		 * if option had been changed, reassigned cron job with a new setting!
		 */
		if ( $old_value != $value ) {

			/**
			 * remove current cron job
			 */
			wp_unschedule_event( wp_next_scheduled( '___fetch_job_event' ), '___fetch_job_event' );

			/**
			 * change interval in order the job will be added in a right way...
			 */
			add_filter( 'cron_schedules', function ( $schedules ) use ( $value ) {
				$schedules['___fetch_job']['interval'] = $value;

				return $schedules;
			} );

			/**
			 * adding the cron job once again with new interval
			 */
			wp_reschedule_event( time(), '___fetch_job', '___fetch_job_event' );
		}
	}
}
