<?php
/**
 * @package: enhr-jp
 */

namespace enhr\Base;


use enhr\Helpers\Log;


class Ajax extends BaseController {

	use Log;

	private $ppp = 25;

	public function __invoke() {

		if ( wp_doing_ajax() ) {

			set_time_limit( 300 );

			add_action( 'wp_ajax_jqxhr', [ $this, 'jqxhr_callbacks' ] );
			add_action( 'wp_ajax_nopriv_jqxhr', [ $this, 'jqxhr_callbacks' ] );
			add_action( 'wp_ajax_jqxhr', 'jqxhr_callbacks' );
			add_action( 'wp_ajax_nopriv_jqxhr', 'jqxhr_callbacks' );
		}
	}


	public function jqxhr_callbacks() {

		$response = $where = [];

		if ( ! empty( $_POST["applies"] ) ) {

			$_POST["table"] = __APPLY_TBL__;

			if ( ! empty( $_POST["applies"]["where"] ) ) {

				foreach ( $_POST["applies"]["where"] as $key => $value ) {

					switch ( $key ) {

						default:
							break;
					}
				}
			}

			if ( ! empty( $where ) ) {
				$where = ' WHERE ' . implode( ' AND ', $where );
			} else {
				$where = '';
			}

			$response = $this->buildLogOutput( 'getAppliesLogRow', $where );

			Log::doJsonOutput( $response );
		}


		/**
		 * Creating tables rows of CPT fetching results from EnHrPublisher
		 */
		if ( ! empty( $_POST["log"] ) ) {

			$_POST["table"] = __LOG_TBL__;

			if ( ! empty( $_POST["log"]["where"] ) ) {

				foreach ( $_POST["log"]["where"] as $key => $value ) {

					switch ( $key ) {

						case 'affected':
							$where[] = "affected > 0";
							break;

						case preg_match( '/^(scope|job)$/', $key, $field ) ? true : false:
							if ( ! empty( $value ) ) {
								$where[] = "{$field[1]} = '{$value}'";
							}
							break;
					}
				}
			}

			if ( ! empty( $where ) ) {
				$where = ' WHERE ' . implode( ' AND ', $where );
			} else {
				$where = '';
			}

			$response = $this->buildLogOutput( 'getEnhrPublisherLogRow', $where );

			Log::doJsonOutput( $response );
		}


		/**
		 * Run fetching data from EnHrPublisher manually
		 * from Dashboard page of the plugin.
		 */
		if ( ! empty( $_POST["sync"] ) ) {

			$cron             = new Cron();
			$response['post'] = $_POST['sync'];
			$response['job']  = $cron->doSync( $_POST['sync'] );

			$cron::unlock();
        
			Log::doJsonOutput( $response );
			wp_die();
		}

		/**
		 * $_POST["apply"]
		 *
		 * This inits globally from Base/Apply::class.
		 * This is just be available via manual submit ar ajax call
		 */
		if ( ! empty( $_POST["apply"] ) ) {
			wp_die();
		}

		wp_die();
	}


	private function buildLogOutput( $method, $where ) {

		$scope = $_POST["scope"];

		$offset = '';

		$limit = $_POST[ $scope ]["limit"] ?? $this->ppp;

		$limit ++;

		if ( ! empty( $_POST[ $scope ]["more"] ) ) {
			$offset = ', ' . $limit;
		}

		$query = "SELECT * FROM {$_POST["table"]}{$where} ORDER by id DESC LIMIT {$_POST[$scope]["initial"]}{$offset}";

		$result = $this->wpdb->get_results( $query, OBJECT );

		$rows = [];

		foreach ( $result as $row ) {

			$rows[] = $this->$method( $row );
		}

		if ( count( $rows ) ) {

			$response['message'] = 'success';

			if ( count( $rows ) > -- $limit ) {

				$response['more'] = 1;
				unset( $rows[ $limit ] );
			}
			$response['body'] = $rows;

		} else {
			$response['message'] = 'error';
			$response['body']    = 'No records found';
		}

		$response['query'] = $query;
		$response['post']  = $_POST;

		return $response;
	}


	/**
	 * Executing dynamically thru `buildLogOutput` method
	 *
	 * @param $row
	 *
	 * @return string
	 */
	private function getEnhrPublisherLogRow( $row ) {
		$log_row = json_decode( $row->log_raw );

		$jobs = [ 'cron' => 'Scheduled', 'manual' => 'Manually' ];

		$tr    = "\t<tr class='logRow'>" . PHP_EOL;
		$tr    .= "\t\t<td>" . $jobs[ $row->job ] . '</td>' . PHP_EOL;
		$tr    .= "\t\t<td>" . $row->scope . '</td>' . PHP_EOL;
		$tr    .= "\t\t<td>" . number_format( $row->end_time - $row->request_time_float, 4, '.', '' ) . '</td>' . PHP_EOL;
		$tr    .= "\t\t<td>" . $row->affected . '</td>' . PHP_EOL;
		$tr    .= "\t\t<td class=\"col-md\">" . $row->description . '</td>' . PHP_EOL;
		$since = $log_row->httpGetQuery->{$row->scope}->since ?? 0;
		$tr    .= "\t\t<td>" . self::convertToHuman( date( 'Y-m-d H:i:s', $since ) ) . ' (' . $log_row->httpGetQuery->{$row->scope}->ordering . ')</td>' . PHP_EOL;
		$tr    .= "\t\t<td>" . self::convertToHuman( $row->logged_gmt ) . '</td>' . PHP_EOL;
		$tr    .= "\t</tr>" . PHP_EOL;

		return $tr;
	}
}
