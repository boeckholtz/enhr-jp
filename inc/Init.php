<?php
/**
 * @package: enhr-jp
 */

namespace enhr;


final class Init {

	/**
	 * Store of the classes inside array
	 * @return array
	 */
	public static function get_services(): array {

		return [
			Pages\Dashboard::class,
			Base\Enqueue::class,
			Base\SettingsLinks::class,
			Base\CustomPostTypeController::class,
			Base\CustomTaxonomyController::class,
			Pages\Logging::class,
			Base\Apply::class,
			Pages\Applying::class,
			Base\Cron::class,
			Base\Ajax::class
		];
	}

	/**
	 * Loop thru the classes, initialize them via __invoke() if exists
	 */
	public static function register_services(): void {

		foreach ( self::get_services() as $class ) {

			$service = self::instantiate( $class );

			if ( method_exists( $service, '__invoke' ) ) {
				$service();
			}
		}
	}

	/**
	 * Initialize the class
	 *
	 * @param $class
	 *
	 * @return mixed
	 */
	private static function instantiate( $class ) {
		return new $class();
	}

}
