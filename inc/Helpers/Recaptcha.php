<?php
/**
 * @package: enhr-jp
 */

namespace enhr\Helpers;


trait Recaptcha {

	public static function getCaptcha( $score = 0.5, $token = null ) {

		$token = $_POST['g-recaptcha-response'] ?? $token;

		if ( $token === null ) {
			return null;
		}

		$response = json_decode( file_get_contents( "https://www.google.com/recaptcha/api/siteverify?secret=" . RECAPTCHA_SECRET_KEY . "&response={$token}" ) );

		if( $response->success === true AND $response->score >= $score ){
			return $response;
		} else {
			return false;
		}
	}

	public static function loadCaptcha()
    {
        $client_token = get_option(__PRFX__.'_apply_captcha_client');
        $content = (string) '';
        if ( ! empty($client_token))
        {
            $content .= sprintf('<script src="https://www.google.com/recaptcha/api.js?render=%s"></script>', $client_token) . "\r\n";
            $content .= '<input type="hidden" name="g-recaptcha-response" id="g-recaptcha-response" value="">';
            $content .= '<script type="text/javascript">
            function getReCaptchaToken() {
                grecaptcha.execute("%s", {action: \'apply_form\'}).then(function (token) {
                  document.getElementById(\'g-recaptcha-response\').value = token;
                });
              }
              
              grecaptcha.ready(function () {
                getReCaptchaToken();
              });
              
              window.setInterval(getReCaptchaToken, 110000);
            </script>
            ';
        }
        return sprintf($content, $client_token);
    }

}