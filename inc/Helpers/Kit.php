<?php
/**
 * @package: enhr-jp
 */

namespace enhr\Helpers;


trait Kit {

	public static function html_attributes( $attributes ) {
		if ( ! $attributes ) {
			return '';
		}

		$compiled = join( '="%s" ', array_keys( $attributes ) ) . '="%s" ';

		return rtrim( vsprintf( $compiled, array_map( 'htmlspecialchars', array_values( $attributes ) ) ) );
	}

	public static function getCaptcha( $score = 0, $token = null ) {

		$token = $_POST['g-recaptcha-response'] ?? $token;

		$response = json_decode( file_get_contents( "https://www.google.com/recaptcha/api/siteverify?secret=" . RECAPTCHA_SECRET_KEY . "&response={$token}" ), true );

		if ( $response['success'] === true ) {
			return $response;
		} else {

			Log::doJsonOutput( [
				'result'  => 'error',
				'message' => ucfirst( str_replace( '-', ' ', $response['error-codes'][0] ) )
			] );
		}
	}

	public static function stats_javascript( $debug = false ) {

		add_action( 'admin_print_footer_scripts', function () use ($debug) {
			$scriptSrc = plugin_dir_url( dirname( __FILE__, 2 ) ) . 'assets/enhr-admin-stats.js';

			if (  $debug !== false ) {
				$scriptSrc .= '?t=' . time();
			}
			?>
            <script>
                let enhrAjaxUrl = '<?=admin_url( 'admin-ajax.php' )?>';
            </script>
            <script type='text/javascript' src='<?= $scriptSrc ?>'></script>
			<?php
		}, 99 );

	}


}