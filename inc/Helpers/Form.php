<?php
/**
 * @package: enhr-jp
 */

namespace enhr\Helpers;

trait Form {

    /**
     * @var array field names are used by the enhr plugin, never ever changes them
     *
     */
    private static $_fields = [
        'firstName',
        'lastNamePrefix',
        'lastName',
        'emailAddress',
        'phoneNumber',
        'CV',
        'gender',
        'motivation',
    ];

    /**
     * @var  array  preferred order of attributes
     */
    public static $attribute_order = [
        'action',
        'method',
        'type',
        'id',
        'name',
        'value',
        'href',
        'src',
        'width',
        'height',
        'cols',
        'rows',
        'size',
        'maxlength',
        'rel',
        'media',
        'accept-charset',
        'accept',
        'tabindex',
        'accesskey',
        'alt',
        'title',
        'class',
        'style',
        'selected',
        'checked',
        'readonly',
        'disabled',
    ];

    /**
     * @var  boolean  use strict XHTML mode?
     */
    public static $strict = true;

    /**
     * Creates a form input. If no type is specified, a "text" type input will
     * be returned.
     *
     *     echo Form::input('username', $username);
     *
     * @param   string  $name       input name
     * @param   string  $value      input value
     * @param   array   $attributes html attributes
     * @return  string
     * @uses    HTML::attributes
     */
    public static function input($name, $value = NULL, array $attributes = NULL)
    {
        // Set the input name
        $attributes['name'] = $name;

        // Set the input value
        $attributes['value'] = $value;

        if ( ! isset($attributes['type']))
        {
            // Default type is text
            $attributes['type'] = 'text';
        }

        if ( ! in_array($name, Form::$_fields))
        {
            throw new Exception($name . ' not allowed. Fieldnames are ' . implode(', ', Form::$_fields));
        }

        return '<input'.Form::attributes($attributes).' />';
    }

    /**
     * Compiles an array of HTML attributes into an attribute string.
     * Attributes will be sorted using HTML::$attribute_order for consistency.
     *
     *     echo '<div'.HTML::attributes($attrs).'>'.$content.'</div>';
     *
     * @param   array   $attributes attribute list
     * @return  string
     */
    public static function attributes(array $attributes = NULL)
    {
        if (empty($attributes))
            return '';

        $sorted = [];
        foreach (Form::$attribute_order as $key)
        {
            if (isset($attributes[$key]))
            {
                // Add the attribute to the sorted list
                $sorted[$key] = $attributes[$key];
            }
        }

        // Combine the sorted attributes
        $attributes = $sorted + $attributes;

        $compiled = '';
        foreach ($attributes as $key => $value)
        {
            if ($value === NULL)
            {
                // Skip attributes that have NULL values
                continue;
            }

            if (is_int($key))
            {
                // Assume non-associative keys are mirrored attributes
                $key = $value;

                if ( ! Form::$strict)
                {
                    // Just use a key
                    $value = false;
                }
            }

            // Add the attribute key
            $compiled .= ' '.$key;

            if ($value OR Form::$strict)
            {
                // Add the attribute value
                $compiled .= '="'.Form::chars($value).'"';
            }
        }

        return $compiled;
    }

    /**
     * Convert special characters to HTML entities. All untrusted content
     * should be passed through this method to prevent XSS injections.
     *
     *     echo HTML::chars($username);
     *
     * @param   string  $value          string to convert
     * @param   boolean $double_encode  encode existing entities
     * @return  string
     */
    public static function chars($value, $double_encode = true)
    {
        return htmlspecialchars( (string) $value, ENT_QUOTES, 'utf-8', $double_encode);
    }

    /**
     * Creates a hidden form input.
     *
     *     echo Form::hidden('csrf', $token);
     *
     * @param   string  $name       input name
     * @param   string  $value      input value
     * @param   array   $attributes html attributes
     * @return  string
     * @uses    Form::input
     */
    public static function hidden($name, $value = NULL, array $attributes = NULL)
    {
        $attributes['type'] = 'hidden';

        return Form::input($name, $value, $attributes);
    }

    /**
     * Creates a file upload form input. No input value can be specified.
     *
     *     echo Form::file('image');
     *
     * @param   string  $name       input name
     * @param   array   $attributes html attributes
     * @return  string
     * @uses    Form::input
     */
    public static function file($name, array $attributes = NULL)
    {
        $attributes['type'] = 'file';

        return Form::input($name, NULL, $attributes);
    }

    /**
     * Creates a checkbox form input.
     *
     *     echo Form::checkbox('remember_me', 1, (bool) $remember);
     *
     * @param   string  $name       input name
     * @param   string  $value      input value
     * @param   boolean $checked    checked status
     * @param   array   $attributes html attributes
     * @return  string
     * @uses    Form::input
     */
    public static function checkbox($name, $value = NULL, $checked = FALSE, array $attributes = NULL)
    {
        $attributes['type'] = 'checkbox';

        if ($checked === TRUE)
        {
            // Make the checkbox active
            $attributes[] = 'checked';
        }

        return Form::input($name, $value, $attributes);
    }

    /**
     * Creates a radio form input.
     *
     *     echo Form::radio('like_cats', 1, $cats);
     *     echo Form::radio('like_cats', 0, ! $cats);
     *
     * @param   string  $name       input name
     * @param   string  $value      input value
     * @param   boolean $checked    checked status
     * @param   array   $attributes html attributes
     * @return  string
     * @uses    Form::input
     */
    public static function radio($name, $value = NULL, $checked = FALSE, array $attributes = NULL)
    {
        $attributes['type'] = 'radio';

        if ($checked === TRUE)
        {
            // Make the radio active
            $attributes[] = 'checked';
        }

        return Form::input($name, $value, $attributes);
    }

    /**
     * Creates a submit form input.
     *
     *     echo Form::submit(NULL, 'Login');
     *
     * @param   string  $name       input name
     * @param   string  $value      input value
     * @param   array   $attributes html attributes
     * @return  string
     * @uses    Form::input
     */
    public static function submit($name, $value, array $attributes = NULL)
    {
        $attributes['type'] = 'submit';

        return Form::input($name, $value, $attributes);
    }

    /**
     * Creates a form label. Label text is not automatically translated.
     *
     *     echo Form::label('username', 'Username');
     *
     * @param   string  $input      target input
     * @param   string  $text       label text
     * @param   array   $attributes html attributes
     * @return  string
     * @uses    HTML::attributes
     */
    public static function label($input, $text = NULL, array $attributes = NULL)
    {
        if ($text === NULL)
        {
            // Use the input name as the text
            $text = ucwords(preg_replace('/[\W_]+/', ' ', $input));
        }

        // Set the label target
        $attributes['for'] = $input;

        return '<label'.HTML::attributes($attributes).'>'.$text.'</label>';
    }

}