<?php
/**
 * @package: enhr-jp
 */

namespace enhr\Helpers;

trait Log {

	public static function execTime() {
		return microtime( true ) - $_SERVER["REQUEST_TIME_FLOAT"];
	}

	/**
	 * @param $input
	 */
	public static function doJsonOutput( $input ): void {

		$json = json_encode( $input, JSON_UNESCAPED_UNICODE, JSON_PRETTY_PRINT );

		if ( ob_end_clean() ) {
			header( 'content-type: application/json; charset=utf-8' );
		}
		echo isset( $_GET['callback'] ) ? "{$_GET['callback']}($json)" : $json;
		die;
	}

	public static function onWpdbError( $errors ) {

		$option = '______wpdb_errors';

		$x = json_decode( get_option( $option ), true );

		array_unshift( $x, $errors );

		$z = array_slice( $x, 0, 5 );

		update_option( $option, json_encode( $z, JSON_PRETTY_PRINT ), false );
	}

	/**
	 * @param $var
	 * @param null $log
	 * @param bool $append
	 */
	public static function dmp( $var, $log = false, $append = false ) {

		if ( is_admin() OR isset( $_REQUEST['dmp'] ) ) {

			if ( $log === false ) {

				echo "<div class=dump style=\"margin-bottom: 12px; padding: 12px; white-space: pre-wrap; background: #42476E !important; font-family: Consolas !important; border: 1px gold dotted !important; color: gold !important; font-size: 15px; padding: 4px 12px !important; border-radius: 4px !important;\">";
				var_dump( $var );
				echo "</div>";

			} else {
				ob_start();
				var_dump( $var );
				$dump = ob_get_clean();

				$prev = '';

				if ( $append ) {
					$prev = get_option( '__YODMP__' ) . '<hr/>';
				}

				$data = $prev . $dump;
				update_option( '__YODMP__', $data );

			}
		}
	}
}