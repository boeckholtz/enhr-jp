<?php
/**
 * @package: enhr-jp
 */

namespace enhr\Helpers;


use Carbon\Carbon;

trait Time {

	public static function getPeriodSelectorUTC( $locale = 'en', $initDate = '1970-01-01' ): void {

		$dt        = Carbon::now( 'UTC' );
		$date_unix = $dt->getTimestamp();
		$dt_today  = $dt->today();
		$today     = $dt_today->getTimestamp();
		$yesterday = $dt->yesterday()->getTimestamp();

		$dt_today->locale( $locale );
		?>

        <label for="periodSelector"><?=__('Selecteer Periode', self::TEXTDOMAIN) ?></label>
        <select id="periodSelector">
            <option value="<?php echo $date_unix - 3600 ?>">1 <?=__('uur', self::TEXTDOMAIN) ?></option>
            <option value="<?php echo $date_unix - 10800 ?>">3<?=__('uur', self::TEXTDOMAIN) ?></option>
            <option value="<?php echo $date_unix - 21600 ?>">6<?=__('uur', self::TEXTDOMAIN) ?></option>
            <option value="<?php echo $date_unix - 43200 ?>">12<?=__('uur', self::TEXTDOMAIN) ?></option>

            <option disabled=""></option>
            <option value="<?php echo $today ?>" selected><?=__('Vandaag', self::TEXTDOMAIN) ?></option>
            <option value="<?php echo $yesterday ?>"><?=__('Gisteren', self::TEXTDOMAIN) ?></option>
            <option value="<?php echo $today - 259200 ?>">3 <?=__('dagen geleden', self::TEXTDOMAIN) ?></option>
            <option value="<?php echo $today - 518400 ?>">6 <?=__('dagen geleden', self::TEXTDOMAIN) ?></option>
            <option value="<?php echo $today - 1036800 ?>">12 <?=__('dagen geleden', self::TEXTDOMAIN) ?></option>
            <option disabled=""></option>
            <?php

			for ( $i = 0; $i <= 2; $i ++ ) {

				$modifier = ( $i === 0 ) ? 0 : 1;

				$t = $dt_today->subMonth( $modifier )->format( 'Y-m-01' );
				$m = ( $i === 0 ) ? __('Dezze maand', self::TEXTDOMAIN) : ucfirst($dt_today->monthName);

				?>
                <option value="<?= strtotime($t) ?>"><?= $m ?></option> <?php
			}
			?>
            <option disabled=""></option>
            <option value="1"><?=__('', self::TEXTDOMAIN) ?>Sinds Begin</option>
        </select>

		<?php
	}

	/**
	 * @param array $in
	 *
	 * @return array
	 */
	public static function convertTimeToDatetime( array $in ): array {

		$out          = [];
		$dt           = Carbon::__set_state( $in );
		$out['local'] = $dt->format( 'Y-m-d H:i:s' );
		$out['local_ts'] = $dt->getTimestamp();

		$utcOffset = $dt->utcOffset();

		$utcOffset < 0 ? $dt->addMinutes( $utcOffset ) : $dt->subMinutes( $utcOffset );
		$out['gmt'] = $dt->format( 'Y-m-d H:i:s' );
		$out['gmt_ts'] = $dt->getTimestamp();

		return $out;
	}

	/**
	 * @param $datetime
	 * @param string $locale
	 * @param string $tz
	 *
	 * @return string
	 */
	public static function convertToHuman( $datetime, $locale = 'en', $tz = 'UTC' ): string {

		$dtz = date_default_timezone_get();
		date_default_timezone_set( $tz );

		//$humanized = Carbon::parse( $datetime )->locale($locale)->diffForHumans( '@' . strtotime( self::getCurrentTimeUTC() ) );
		$humanized = Carbon::parse( $datetime )->locale( $locale )->longAbsoluteDiffForHumans();

		date_default_timezone_set( $dtz );

		return $humanized;
	}

	/**
	 * @return string
	 */
	public static function getCurrentTimeUTC(): string {

		$time = self::convertTimeToDatetime( [
			"date"          => date( 'Y-m-d H:i:s' ),
			"timezone_type" => 3,
			"timezone"      => date_default_timezone_get()
		] );

		return $time['gmt'];
	}


}