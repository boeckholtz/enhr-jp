<?php
/**
 * @package: enhr-jp
 */

namespace enhr\Helpers;


trait MultiLingual {

	public static function getCodes() {

		return array_keys( apply_filters( 'wpml_active_languages', null ) );
	}

	public static function getActiveLanguages() {

		return apply_filters( 'wpml_active_languages', null );
	}

	public static function getLanguages() {

		if ( $code = apply_filters( 'wpml_default_language', null ) ) {

			$wpml = [];

			$codes = array_keys( apply_filters( 'wpml_active_languages', null ) );

			$wpml['plugin_name']           = 'WPML Multilingual CMS';
			$wpml['languages_codes']       = $codes;
			$wpml['default_language_code'] = $code;
			$wpml['query_language_codes']  = implode( ',', array_unique( array_merge( [ $code ], $codes ) ) );

			return $wpml;
		}

		$lang = get_option( __PRFX__ . '_default_language_code' );
		$lang = ! empty( $lang ) ? $lang : get_locale();

		return substr( $lang, 0, 2 );
	}
}