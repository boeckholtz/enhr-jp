<?php
/**
 * @package: enhr-jp
 */

namespace enhr\Helpers;


trait Filter {

	public static
		$min_int = 30,
		$max_int = DAY_IN_SECONDS,
		$default_int = 600,
	    $default_slug = 'vacature';


	public static function integer( $input ) {
		return filter_var( $input, FILTER_VALIDATE_INT, [
				'options' => [
					'default'   => self::$default_int,
					'min_range' => self::$min_int,
					'max_range' => self::$max_int
				]
			]
		);
	}

	public static function string($input) {
	    return filter_var($input, '', [
	       'options' => [
	           'default' => self::$default_slug
           ]
        ]);
    }

	public static function tinyInt( $input ) {

		return filter_var( $input, FILTER_VALIDATE_INT, [
				'options' => [
					'min_range' => 1
				]
			]
		);
	}

	public static function checkboxSanitize( $input ) {

		//return filter_var( $input, FILTER_SANITIZE_NUMBER_INT );
		return ( isset( $input ) ? true : false );
	}

	public static function languageCode( $input ) {

		$code =  filter_var($input, FILTER_VALIDATE_REGEXP, [
				"options" => ["regexp"=>"/^[a-z]{2}$/i"]
			]
		);

		return $code !== false ? $code : substr( get_locale(), 0, 2);
	}

	/**
	 * @flags FILTER_FLAG_PATH_REQUIRED|FILTER_FLAG_QUERY_REQUIRED
	 *
	 * @param $input
	 * @param null $flags
	 *
	 * @return mixed
	 */
	public static function url( $input, $flags = null ) {

		$options = [
			'options' => [ 'default' => '' ],
			'flags' => $flags
		];

		return filter_var( $input, FILTER_VALIDATE_URL, $options);
	}
}