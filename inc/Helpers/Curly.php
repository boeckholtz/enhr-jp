<?php
/**
 * @package: enhr-jp
 */

namespace enhr\Helpers;


use enhr\Base\BaseController;

class Curly extends BaseController {

	private
		$auth_counter = 0;

	public
		$curl,
		$header = [],
		$url,
		$httpGetQuery,
		$endPoint,
		$jwt,
		$apply_post_fields,
		$mime = 'application/json';

	public function __construct() {

		parent::__construct();

		$this->curl = curl_init();

		$this->header['upgrade']     = "Upgrade-Insecure-Requests: 1";
		$this->header['accept']      = "Accept: " . $this->mime;
		$this->header['bearer']      = "Authorization: Bearer " . @$this->api_access_token["access_token"];
		//Commented below line because of switch to new production server
		//$this->header['compression'] = "Accept-Encoding: gzip, deflate, br";

		curl_setopt( $this->curl, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $this->curl, CURLOPT_TIMEOUT, 15 );
		curl_setopt( $this->curl, CURLOPT_USERAGENT, get_bloginfo( 'name' ) . '/WP' . get_bloginfo( 'version' ) );
		curl_setopt( $this->curl, CURLOPT_SSL_VERIFYHOST, false );
		curl_setopt( $this->curl, CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $this->curl, CURLOPT_HEADER, true );
		curl_setopt( $this->curl, CURLOPT_CONNECTTIMEOUT, 15 );
		curl_setopt( $this->curl, CURLOPT_HTTPHEADER, $this->header );
	}

	private function buildQuery() {
		return key( $this->httpGetQuery ) . '?' . http_build_query( current( $this->httpGetQuery ), PHP_QUERY_RFC1738 );
	}

	private function auth( $method = 'httpGet' ) {

		curl_setopt( $this->curl, CURLOPT_HTTPHEADER, [ "Content-Type: " . $this->mime ] );
		curl_setopt( $this->curl, CURLOPT_URL, get_option( __PRFX__ . '_rest_api_request_token_url' ) );
		curl_setopt( $this->curl, CURLOPT_POST, true );
		curl_setopt( $this->curl, CURLOPT_POSTFIELDS, self::jwt() );

		$resp = $this->executeResponse( $method );

		if ( $resp["info"]["http_code"] == 200 ) {
			$json = json_decode( $resp["content"], true );

			if ( ! empty ( $json["access_token"] ) ) {

				if ( update_option( __PRFX__ . '_access_token', $resp["content"] ) ) {
					$this->log['http']['access_token'] = 'Updated';
					$this->header['bearer']            = "Authorization: Bearer " . $json["access_token"];
					curl_setopt( $this->curl, CURLOPT_HTTPHEADER, $this->header );

					return $this->{$method}();
				}
			}
		}

		return false;
	}

	public function httpGet() {

		if ( empty( $this->httpGetQuery ) ) {
			return false;
		}

		$this->url = $this->api_base_url . $this->buildQuery();

		curl_setopt( $this->curl, CURLOPT_URL, $this->url );

		return $this->executeResponse( substr( strrchr( __METHOD__, "::" ), 1 ) );
	}

	public function setApply() {

		$this->header['content'] = "Content-Type: application/json";
		$this->header['accept']  = "Accept: application/json";
		curl_setopt( $this->curl, CURLOPT_HTTPHEADER, $this->header );

		curl_setopt( $this->curl, CURLOPT_URL, $this->api_base_url . $this->endPoint );
		curl_setopt( $this->curl, CURLOPT_POST, true );
		curl_setopt( $this->curl, CURLOPT_POSTFIELDS, $this->apply_post_fields );

		return $this->executeResponse( substr( strrchr( __METHOD__, "::" ), 1 ) );
	}


	private function validateResponse( $resp, $method ) {

		if ( ++ $this->auth_counter >= 3 ) {
			$this->log['Unauthenticated'] = $this->auth_counter;

			return false;
		}
		switch ( $resp["info"]["http_code"] ) {
			case 200:
				$this->log['http']['Authenticated'] = -- $this->auth_counter;
				$this->auth_counter                 = 0;

				return $resp;
			case 401:
				$this->log['http']['Unauthenticated'] = -- $this->auth_counter;

				return $this->auth( $method );
			default:
				$this->log['http']['code'] = $resp["info"]["http_code"];

				$resp = json_decode( $resp["content"], true );

				if ( ! empty( $resp["errors"] ) ) {

					$this->log['curl_exit_msg'] = '';

					foreach ( $resp["errors"] as $where => $errors ) {
						$this->log['curl_exit_msg'] .= '<strong>' . strtoupper($where) . ':</strong> ' . implode( '; ', $errors ) . '</br>';
					}

				} elseif ( ! empty( $resp["message"] ) ) {
					$this->log['curl_exit_msg'] = $resp["message"];
				} else {
					$this->log['curl_exit_msg'] = __( 'HTTP error code: ', self::TEXTDOMAIN ) . $resp["info"]["http_code"];
				}

				return false;
		}
	}

	private function executeResponse( $method ) {

		$resp            = [ 'data' => curl_exec( $this->curl ) ];
		$resp['err']     = curl_errno( $this->curl );
		$resp['$errmsg'] = curl_error( $this->curl );
		$resp['info']    = curl_getinfo( $this->curl );

		$header_content = substr( $resp['data'], 0, $resp['info']['header_size'] );
		$body_content   = trim( str_replace( $header_content, '', $resp['data'] ) );

		$output               = [ 'errno' => $resp['err'] ];
		$output['errmsg']     = $resp['$errmsg'];
		$output['headers']    = $header_content;
		$output['parsed_url'] = parse_url( $this->url );
		$output['pathinfo']   = pathinfo( $output['parsed_url']["path"] );
		$output['info']       = $resp['info'];
		$output['content']    = $body_content;
		return $this->validateResponse( $output, $method );
	}

	private static function jwt() {
		return json_encode( [
			"grant_type"    => "client_credentials",
			"client_id"     => (string) get_option( __PRFX__ . '_rest_api_client_id' ),
			"client_secret" => get_option( __PRFX__ . '_rest_api_client_secret_id' ),
			"scope"         => "*"
		], JSON_PRETTY_PRINT );
	}

	public function __destruct() {
		curl_close( $this->curl );
	}
}
