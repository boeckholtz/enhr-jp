<?php
/**
 * @package: enhr-jp
 */

namespace enhr;

class Deactivate {

	public static function deactivate() {
		flush_rewrite_rules();
	}
}
