<?php
/**
 * @package: enhr-jp
 */

namespace enhr\Api\Callbacks;



use enhr\Base\BaseController;

class AdminCallbacks extends BaseController {

	public function adminGeneral() {

		return require_once $this->templates . 'general.php';
	}

	public function adminLogging() {

		return require_once $this->templates . 'logging.php';
	}

	public function adminApplying() {

		return require_once $this->templates . 'applies.php';
	}

	public function generalRestApiUrl() {

		echo '<input type="text" class="regular-text" name="' . __PRFX__ . '_rest_api_url" value="' . esc_attr( get_option( __PRFX__ . '_rest_api_url' ) ) . '" placeholder="Specify URL">';
	}

	public function generalRestApiClientID() {

		echo '<input type="text" class="regular-text" name="' . __PRFX__ . '_rest_api_client_id" value="' . esc_attr( get_option( __PRFX__ . '_rest_api_client_id' ) ) . '" placeholder="Specify ID">';
	}
}