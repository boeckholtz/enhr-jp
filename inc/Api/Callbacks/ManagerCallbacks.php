<?php
/**
 * @package: enhr-jp
 */

namespace enhr\Api\Callbacks;


use enhr\Base\BaseController;
use enhr\Helpers\Kit;
use enhr\Helpers\Log;

class ManagerCallbacks extends BaseController {

	public function hashSanitize( $input ) {

		return $input;
	}

	public function publisherSectionManager() {


		?>
		<div id="publisherManagerWrapper">
            <h1 class="section-title"><?=__('Handmatige synchronisatie', self::TEXTDOMAIN) ?></h1>
            <label for="postTypeSelector"><?=__('Selecteer Data', self::TEXTDOMAIN) ?></label>
            <select id="postTypeSelector">
                <option value="Company|Vacancy"><?=__('Bedrijven', self::TEXTDOMAIN) ?> / <?=__('Vacatures', self::TEXTDOMAIN) ?></option>
                <option value="Company"><?=__('Bedrijven', self::TEXTDOMAIN) ?></option>
                <option value="Vacancy"><?=__('Vacatures', self::TEXTDOMAIN) ?></option>
            </select>
			<?php
			$dateInitial = $this->wpdb->get_col( "SELECT DATE(post_date_gmt) FROM {$this->wpdb->prefix}posts WHERE post_type REGEXP '^(company|vacancy)$' AND  post_status = 'publish' ORDER BY post_date_gmt ASC LIMIT 1" );

			$initDate = $dateInitial[0] ?? '1970-01-01';

			self::getPeriodSelectorUTC( $this->primaryLanguage, $initDate);
			?>
            <input id="enhrSyncBtn" type="button" value="<?=__('Synchroniseren', self::TEXTDOMAIN) ?>">
            <listing id="syncResponse"></listing>

			<?php

			$debug_log = WP_CONTENT_DIR . '/debug.log';

			if ( ! empty( WP_DEBUG_LOG ) AND file_exists( $debug_log ) ) { ?>

                <details>
                    <summary>Debug log files</summary>

                    <textarea id="debugLog" style="width: 100%; height: 480px"><?php

						exec( 'tail -128 ' . $debug_log, $output );

						krsort( $output );

						echo implode( PHP_EOL, $output );
						?>
                    </textarea>
                </details>

			<?php } ?>
        </div>
        <code id="dump_response" style="white-space: pre-wrap; display: none;"></code>
		<?php
	}

	public function cron_user() {
		echo '<strong>' . self::$user->display_name . '</strong>';

	}

	public function langCodeSelector( $args ) {

		$input     = esc_attr( get_option( $args['label_for'] ) );
		$languages = self::getActiveLanguages();

		if ( ! empty( $languages ) ) {

			echo "<select name=\"{$args['label_for']}\">";

			foreach ( $languages as $language ) {
				echo '<option value="' . ( $language['code'] == $input ? $input : $language['code'] ) . '"' . ( $language['code'] == $input ? ' selected' : null ) . '>' . $language['native_name'] . '</option>' . PHP_EOL;
			}

			echo "</select>";

		} else {

			echo "<input type=\"text\" name=\"{$args['label_for']}\" maxlength=\"2\" value=\"$input\"  pattern=\"[a-z]{2}\" maxlength=\"2\" required>";
		}
	}

	public function inputField( $args ) {

		$type  = $args['type'] ?? 'text';
		$input = esc_attr( get_option( $args['label_for'] ) );
        echo "<input type=\"$type\" name=\"{$args['label_for']}\" value=\"$input\"" . ( !empty($args['attr']) ? ' ' . Kit::html_attributes($args['attr']) : '' )  . ">";
	}

	public function inputCheckbox( $args ) {
		$input = esc_attr( get_option( $args['label_for'] ) );

		echo "<input id=\"{$args['label_for']}\" type=\"checkbox\" name=\"{$args['label_for']}\" value=\"" . ( empty( $input ) ? 0 : $input ) . "\"" . ( empty( $input ) ? null : ' checked' ) . ">";

		if ( ! empty( $args['label_text'] ) ) {
			echo "<label for=\"{$args['label_for']}\" style='padding-left: .5rem;" . ( $input != 1 ? '' : ' font-weight: bold;' ) . "'>{$args['label_text']}</label>";
		}
	}


}
