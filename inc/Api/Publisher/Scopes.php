<?php
/**
 * @package: enhr-jp
 */

namespace enhr\Api\Publisher;


interface Scopes {

	public function run( $args );
	public function insertPostData();

}