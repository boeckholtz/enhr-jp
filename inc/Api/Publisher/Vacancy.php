<?php
/**
 * @package: enhr-jp
 */

namespace enhr\Api\Publisher;


class Vacancy extends Job implements Scopes {

	public function run( $args = [] ) {

		$this->deletePosts      = true;
		$this->args             = $args;
		$this->scope            = 'vacancies';
		$this->post_type        = 'vacancy';
		$this->scope_taxonomies = [
			'country',
			'location',
			'department_primary',
			'department_secondary',
			'education_primary',
			'education_secondary',
			'education_tertiary'
		];

		$this->preparation();
		$this->fetcher();
        $this->deletePosts();

		return $this->postAction();
	}
}
