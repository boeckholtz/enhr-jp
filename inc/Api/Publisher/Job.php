<?php
/**
 * @package: enhr-jp
 */

namespace enhr\Api\Publisher;

use Carbon\Carbon;
use enhr\Helpers\Curly;
use enhr\Helpers\Log;

class Job extends Curly {

    protected
        $counter = 0,
        $companyLogoAttachedID = [],
        $post,
        $postID,
        $translations = [],
        $user_meta_fields_2_translate = [],
        $language_to_translate,
        $primary_post_id,
        $translate_post_id,
        $mergedPostData,
        $saveLogo = false,    /* Never change this value */
        $deletePosts = false; /* Never change this value */

    public
        $currentUser,
        $args,
        $job = 'cron',
        $lastCptId,
        $system_time,
        $post_status,
        $logDescription = '',
        $post_type,
        $post_data = [],
        $taxonomies = [],
        $metas = [],
        $meta_key_prefix,
        $httpGetQuery,
        $httpResponseMeta,
        $httpResponse,
        $scope,
        $scope_taxonomies = [];

    /**
     * @param Scopes $scope
     * @param array $args
     *
     * @return array
     */
    public function init( Scopes $scope, $args = [] ) {
        return $scope->run( $args );
    }

    protected function preparation() {

        $this->meta_key_prefix = __PRFX__ . '_' . $this->post_type . '_';
        $this->job             = $this->args["job"] ?? $this->job;

        $tz = date_default_timezone_get();

        $this->system_time = self::convertTimeToDatetime( [
            "date"          => date( 'Y-m-d H:i:s' ),
            "timezone_type" => 3,
            "timezone"      => $tz
        ] );

        date_default_timezone_set( 'UTC' );

        if ( empty( $this->args["since"] ) ) {
            $post_modified_gmt = $this->wpdb->get_col( "SELECT post_modified_gmt FROM {$this->wpdb->prefix}posts WHERE post_type = '{$this->post_type}' AND  post_status = 'publish' ORDER BY post_modified_gmt DESC LIMIT 1" )[0];
            $since             = strtotime( $post_modified_gmt ) - ( (int) get_option( __PRFX__ . '_cron_interval' ) / 3 );
        } else {
            $since = $this->args["since"];
        }

        $this->httpGetQuery = [
            $this->scope => [
                'count'    => get_option( __PRFX__ . '_cron_portion' ),
                'page'     => 1,
                'ordering' => 'latest',
                'langs'    => $this->query_language_codes
            ]
        ];

        $this->log['scope']             = $this->scope;
        $this->log['httpGetQuery']      = $this->httpGetQuery;
        $this->log['can_deleted_posts'] = current_user_can( 'delete_others_posts' );
        $this->log['inserted']          = [];
        $this->log['inserted_errors']   = [];
        $this->log['updated']           = [];
        $this->log['deleted']           = [];
        $this->log['is_deleted']        = [];
        $this->log['undeleted']         = [];
        $this->log['is_undeleted']      = [];
        $this->log['non_disposable_root_ids'] = [];

        date_default_timezone_set( $tz );
    }


    /**
     * The main method, Initiate the fetching date from enpublisher Api via httpGet() extended from Curly.class
     * The method calls vai classes Company/Vacancy extending Job class that implements Scopes Interface class
     */
    protected function fetcher(): void {

        $this->httpResponse = $this->httpGet();

        if ( $this->validateHttpResponse() ) {

            if ( $this->httpResponseMeta["total_pages"] > 0 ) {

                $this->log['meta'] = $this->httpResponseMeta;
                $this->insertPostData();

                if ( isset( $this->httpResponse[0] ) ) {
                    Log::dmp( $this->httpResponse[0], true, true );
                }

                /**
                 * Iterating the rest of the results based on option with key __PRFX__ . '_cron_portion'
                 * keeping the numbers or result per page
                 */
                for ( $p = 1; $p < $this->httpResponseMeta["total_pages"]; $p ++ ) {

                    $this->httpGetQuery[ $this->scope ]['page'] = $this->httpResponseMeta["current_page"] + 1;

                    $this->httpResponse = $this->httpGet();

                    if ( $this->validateHttpResponse() ) {
                        $this->insertPostData();
                    }
                }
            }
        }
    }

    /**
     * @return bool
     *
     * Validating the answer from enpublisher
     * on false executing $this->postAction()
     */
    public function validateHttpResponse(): bool {

        if ( $this->httpResponse !== false ) {

            $resp                   = json_decode( $this->httpResponse["content"], true );
            $this->httpResponseMeta = $resp['meta']['pagination'];
            $this->httpResponse     = $resp['data'];

            return count( $this->httpResponse ) > 0;
        }

        return false;
    }

    /**
     * Saving the image to the local folder if any
     */
    private function saveCompanyLogo(): int {
        if ( $this->saveLogo AND $this->post_type == 'company' AND ! empty( $this->post['logo'] ) AND ! empty( $this->post['logo_content'] ) ) {
            $filename = 'company_' . strtolower( sanitize_file_name( $this->mergedPostData['post_title'] ) ) . '_' . pathinfo( $this->post['logo'], PATHINFO_BASENAME );

            $thumbnailUrl = get_the_post_thumbnail_url( $this->primary_post_id, 'full' );

            $date = Carbon::__set_state( $this->post['created_at'] )->format( 'Y/m' );

            $thumbnail_id  = get_post_thumbnail_id( $this->primary_post_id );
            $getpath       = get_attached_file( $thumbnail_id, 'full' );

            if ( $thumbnail_id && get_post_type($thumbnail_id) == 'attachment' ) {
                $isCurrentFile = pathinfo( $getpath, PATHINFO_DIRNAME ) . DIRECTORY_SEPARATOR . $filename;
                try{
                    //unlink( $isCurrentFile );
                    wp_delete_attachment($thumbnail_id, true);
                }catch(Exception $err){

                }
            }

            $upload   = wp_upload_bits( $filename, null, base64_decode( $this->post['logo_content'] ), $date );
            $fileType = wp_check_filetype( $upload['file'], null );
            $attachment = array(
                'post_mime_type' => $fileType['type'],
                'post_title'     => $filename,
                'post_content'   => '',
                'post_status'    => 'inherit'
            );

            $attachmentId = wp_insert_attachment( $attachment, $upload['file'], $this->primary_post_id );

            require_once( ABSPATH . 'wp-admin/includes/image.php' );
            $attached_data = wp_generate_attachment_metadata( $attachmentId, $upload['file'] );
            wp_update_attachment_metadata( $attachmentId, $attached_data );

            if ( set_post_thumbnail( $this->primary_post_id, $attachmentId ) ) {
                $this->log['inserted_logo']['name']          = $upload['file'];
                $this->log['inserted_logo']['attachment_id'] = $attachmentId;

                return $attachmentId;
            }
        }

        return 0;
    }

    /**
     * @return array
     *
     * Another important method that actually insert the posts and theirs taxonomies, meta values,
     * and registering the corresponding translations if any
     */
    public function insertPostData() {
        
        foreach ( $this->httpResponse as $this->post ) {

            if ( $this->parseTranslations() === true ) {

                if ( $this->preparePostData() === false ) {

                    continue;
                }

                // $this->mergePostData = $this->mergePostData( $this->primaryLanguage, $this->postID );

                /**
                 * @bug
                 * Changed mergePostData to mergedPostData
                 */

                $this->mergedPostData = $this->mergePostData( $this->primaryLanguage, $this->postID );
                $this->primary_post_id = wp_insert_post( wp_slash( $this->mergedPostData ), true );

                if ( is_wp_error( $this->primary_post_id ) ) {

                    $this->log['inserted_errors'][ $this->post['root_id'] . ':' . $this->primaryLanguage ] = $this->primary_post_id->get_error_message();
                }

                $this->setObjectTerms( $this->primary_post_id, 'tax_input' );
                $this->companyLogoAttachedID = $this->saveCompanyLogo();
                $this->log['inserted_errors'][] = $this->companyLogoAttachedID;

                if ( $this->primary_post_id > 0 ) {

                    $this->log[ $this->post_status ][] = $this->primary_post_id;

                    unset( $this->translations[ $this->primaryLanguage ] );

                    if ( ! empty( $this->translations ) AND $this->isMultiLingual ) {

                        global $sitepress;

                        $taxonomies = wp_get_post_terms( $this->primary_post_id, $this->scope_taxonomies, [ 'fields' => 'all' ] );

                        foreach ( $this->translations as $this->language_to_translate => $data ) {

                            // REWRITE CODE BELOW
                            // PURPOSE: ADD MULTILINGUAL CONTENT AND RELATE TO ORIGINAL POST

                            $this->translate_post_id = apply_filters( 'wpml_object_id', $this->primary_post_id, 'post', false, $this->language_to_translate );

                            $this->mergedPostData     = $this->mergePostData( $this->language_to_translate, $this->translate_post_id );
                            $this->translate_post_id = wp_insert_post( wp_slash( $this->mergedPostData ), true );

                            if ( is_wp_error( $this->translate_post_id ) ) {

                                $this->log['inserted_errors'][ $this->post['root_id'] . ':' . $this->language_to_translate ] = $this->translate_post_id->get_error_message();
                            }

                            if ( ! empty ( $this->translate_post_id ) ) {

                                $this->setObjectTerms( $this->translate_post_id, 'tax_input' );

                                if ( $this->companyLogoAttachedID > 0 ) {
                                    set_post_thumbnail( $this->translate_post_id, $this->companyLogoAttachedID );
                                }

                                $trid = $sitepress->get_element_trid( $this->primary_post_id, 'post_' . $this->post_type );

                                $sitepress->set_element_language_details(
                                    $this->translate_post_id,
                                    'post_' . $this->post_type,
                                    $trid,
                                    $this->language_to_translate,
                                    $this->primaryLanguage
                                );

                                if ( ! empty( $taxonomies ) ) {

                                    foreach ( $taxonomies as $taxonomy ) {

                                        if ( empty( apply_filters( 'wpml_object_id', $taxonomy->term_id, $taxonomy->taxonomy, true, $this->language_to_translate ) ) ) {

                                            $element_type = apply_filters( 'wpml_element_type', $taxonomy->taxonomy );
                                            $trid         = $sitepress->get_element_trid( $taxonomy->term_id, $element_type );

                                            $sitepress->set_element_language_details(

                                                wp_get_post_terms( $this->translate_post_id, $taxonomy->taxonomy )[0]->term_id,
                                                $element_type,
                                                $trid,
                                                $this->language_to_translate,
                                                $this->primaryLanguage
                                            );
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return [];
    }

    /**
     * @return bool
     *
     * Detecting the languages and parsing the translations due to valid format to be used later
     * If false $this->postAction() will be performed
     */
    public function parseTranslations(): bool {

        $return = false;

        foreach ( $this->post["translations"]["data"] as $translation ) {

            $code = $translation["language_code"];

            $languages[] = $code;

            if ( $translation["language_code"] == $this->primaryLanguage ) {
                $return = true;
            }

            //unset( $translation["language_code"] );

            if ( $this->scope === 'companies' ) {

                $this->translations[ $code ]['body']['post_title'] = $translation["name"];
                unset( $translation["name"] );

                if ( ! empty( $translation["information"] ) ) {
                    $this->translations[ $code ]['body']['post_excerpt'] = trim( strip_tags( $translation["information"] ) );
                }

                unset( $translation["information"] );
            }


            if ( $this->scope === 'vacancies' ) {

                $this->translations[ $code ]['body']['post_title'] = $translation["title"];
                unset( $translation["title"] );

                if ( ! empty( $translation["intro"] ) ) {
                    $this->translations[ $code ]['body']['post_excerpt'] = trim( $translation["intro"] );
                }

                if ( ! empty( $translation["tags"] ) ) {
                    $this->translations[ $code ]['body']['tags_input'] = explode( ',', $translation["tags"] );
                }
                unset( $translation["tags"] );
            }

            if ( ! empty( $this->translations[ $code ]['body']['tax_input'] ) ) {
                unset( $this->translations[ $code ]['body']['tax_input'] );
            }

            foreach ( $this->scope_taxonomies as $taxonomy ) {

                if ( ! empty( $translation[ $taxonomy ] ) ) {
                    $this->translations[ $code ]['body']['tax_input'][ $taxonomy ][] = $translation[ $taxonomy ];
                }

                unset( $translation[ $taxonomy ] );
            }

            foreach ( $translation as $key => $value ) {
                $this->user_meta_fields_2_translate[]                                 = $this->meta_key_prefix . $key;
                $this->translations[ $code ]['meta'][ $this->meta_key_prefix . $key ] = $value;
            }
        }


        return $return;
    }

    /**
     * Pre-composing internal WP format arrays to insert into DB;
     */
    public function preparePostData(): bool {

        $this->postID = $this->isID();

        /**
         * Checking id post should be deleted or not,
         * Push the id to the array for deletion
         */
        if ($this->deletePosts === true AND ! empty( $this->post['deleted_at'] ) AND is_array( $this->post['deleted_at'] ) ) {

            $this->log['deleted'][] = $this->postID;

            return false;
        }

        if($this->scope == 'vacancies'){
            $this->log['non_disposable_root_ids'][] = $this->post['root_id'];
        }

        $created = self::convertTimeToDatetime( $this->post['created_at'] );
        $updated = self::convertTimeToDatetime( $this->post['updated_at'] );

        $this->post_data['post_author']       = $this->active_user;
        $this->post_data['post_date']         = $created['local'];
        $this->post_data['post_date_gmt']     = $created['gmt'];
        $this->post_data['post_status']       = 'publish';
        $this->post_data['comment_status']    = 'closed';
        $this->post_data['ping_status']       = 'closed';
        $this->post_data['post_modified']     = $updated['local'];
        $this->post_data['post_modified_gmt'] = $updated['gmt'];
        $this->post_data['post_type']         = $this->post_type;

        $this->metas[ $this->meta_key_prefix . 'root_id' ]      = $this->post['root_id'];

        if ( $this->scope === 'companies' ) {
            $this->metas[ $this->meta_key_prefix . 'email' ]        = $this->post['email'];
            $this->metas[ $this->meta_key_prefix . 'phone_number' ] = $this->post['phone_number'];
            $this->metas[ $this->meta_key_prefix . 'url' ]          = $this->post['url'];
            $this->metas[ $this->meta_key_prefix . 'coord_x' ]      = $this->post['coord_x'];
            $this->metas[ $this->meta_key_prefix . 'coord_y' ]      = $this->post['coord_y'];
        }

        if ( $this->scope === 'vacancies' ) {
            $this->metas[ $this->meta_key_prefix . 'company_root_id' ]       = $this->post['company_root_id'];
            $this->metas[ $this->meta_key_prefix . 'apply_url' ]             = $this->post['apply_url'];
            $this->metas[ $this->meta_key_prefix . 'publication_start_gmt' ] = self::convertTimeToDatetime( $this->post['publication_start'] )['gmt'];
            $this->metas[ $this->meta_key_prefix . 'publication_end_gmt' ]   = self::convertTimeToDatetime( $this->post['publication_end'] )['gmt'];
            $this->metas[ $this->meta_key_prefix . 'salary_indication' ]     = $this->post['salary_indication'];
            $this->metas[ $this->meta_key_prefix . 'salary_min' ]            = $this->post['salary_min'];
            $this->metas[ $this->meta_key_prefix . 'salary_max' ]            = $this->post['salary_max'];
            $this->metas[ $this->meta_key_prefix . 'closed' ]                = $this->post['closed'];
            $this->metas[ $this->meta_key_prefix . 'deleted' ]               = $this->post['deleted'];
            $this->metas[ $this->meta_key_prefix . 'status' ]                = $this->post['status'];
            $this->metas[ $this->meta_key_prefix . 'medium' ]                = $this->post['medium'];
            $this->metas[ $this->meta_key_prefix . 'vacancy_id' ]            = $this->post['vacancy_id'];
            $this->metas[ $this->meta_key_prefix . 'publication_id' ]        = $this->post['publication_id'];
            $this->metas[ $this->meta_key_prefix . 'hours_per_week' ]        = $this->post['hours_per_week'];
            $this->metas[ $this->meta_key_prefix . 'hours_per_week_custom' ] = $this->post['hours_per_week_custom'];
            $this->metas[ $this->meta_key_prefix . 'open_application' ]      = $this->post['open_application'] ? '1' : '0';
        }

        $this->post_data['meta_input'] = $this->metas;

        return true;
    }

    /**
     * @param $lang
     * @param $ID
     *
     * @return array
     *
     * Merging the translated data with common port into solid array for Inserting/Updating to the DB
     */
    private function mergePostData( $lang, $ID ): array {

        $postData = $this->post_data;

        if ( $ID != null ) {
            $postData['ID']    = $ID;
            $this->post_status = 'updated';
        } else {
            $this->post_status = 'inserted';
        }

        $postData["meta_input"] = array_merge( $postData["meta_input"], $this->translations[ $lang ]['meta'] );

        return array_merge( $postData, $this->translations[ $lang ]['body'] );
    }


    /**
     * @param $id
     * @param $terms
     *
     * The native wp_insert_post() functions is working only for the registering users
     * Registering taxonomies in cron-jobs must be performed separately
     */
    public function setObjectTerms( $id, $terms ) {

        if ( $id > 0 AND ! empty( $this->mergedPostData[ $terms ] ) ) {

            foreach ( $this->mergedPostData[ $terms ] as $tax => $values ) {
                wp_set_object_terms( $id, wp_slash( $values ), $tax );
            }
        }
    }

    /**
     * @return mixed
     *
     * Checking if received records had been already stored into DB
     * in order to update the date, used mainly in $this->>preparePostData()
     */
    public function isID() {

        $args = array(
            'posts_per_page' => 1,
            'post_type'      => $this->post_type,
            'meta_query'     => [
                [
                    'key'   => $this->meta_key_prefix . 'root_id',
                    'value' => $this->post['root_id'],
                    'type'  => 'INT'
                ],
                [
                    'key'   => $this->meta_key_prefix . 'language_code',
                    'value' => $this->primaryLanguage
                ]
            ],
            'fields'         => 'ids'
        );

        $query = new \WP_Query( $args );

        return $query->posts[0] ?? null;
    }


    /**
     * @return array
     *
     * Finalizing the process.
     * Returning the log for manual execution to display the result
     */
    public function postAction(): array {
        return $this->insertLog();
    }

    /**
     * Get Post ID's for all vacancies that need to be deleted.
     */
    public function getDisposables(){

        return $this->wpdb->get_results("
	        SELECT ID 
	          FROM ".$this->wpdb->prefix."posts p	         
	          JOIN ".$this->wpdb->prefix."postmeta pm
	               ON (p.ID = pm.post_id)
	               
	         WHERE p.post_type = 'vacancy' 
	           AND p.post_status = 'publish'
	           AND pm.meta_key = 'enhr_vacancy_root_id'
	           AND pm.meta_value NOT IN ('', '".implode("','", $this->log['non_disposable_root_ids'])."')
	          ");
    }


    /**
     * Deleting the post and translations links if WPML used
     */
    protected function deletePosts(): void {
        
        $deleted        = [];
        $undeleted      = [];
        $list_to_delete = [];

        if($this->scope === 'vacancies'){
            $disposableVacancies = $this->getDisposables();
            if($disposableVacancies){
                foreach($disposableVacancies as $dsp){
                    if(!in_array($dsp->ID, $this->log['deleted'])){
                        $this->log['deleted'][] = $dsp->ID;
                    }
                }
            }
        }

        if ( function_exists( 'icl_get_languages' ) ) {

            global $sitepress;

            foreach ( $this->log['deleted'] as $deleted_id ) {

                $trid         = $sitepress->get_element_trid( $deleted_id, 'post_vacancy' );
                $translations = $sitepress->get_element_translations( $trid );

                foreach ( $translations as $translation ) {

                    $list_to_delete[] = $translation->element_id;
                }
            }

        } else {

            $list_to_delete = $this->log['deleted'];
        }

        foreach ( $list_to_delete as $deleted_id ) {
            $post = get_post($deleted_id);
            $deleted_post = wp_delete_post( $deleted_id, true );

            if ( ! empty( $deleted_post ) ) {
                $deleted[] = $deleted_id;
            } else {
                $undeleted[] = $deleted_id;
            }
        }

        $this->log['is_deleted']   = $deleted;
        $this->log['is_undeleted'] = $undeleted;
    }

    /**
     * @return array
     *
     * No comments :)
     */
    public function insertLog(): array {

        $inserted = count( $this->log['inserted'] );
        $updated  = count( $this->log['updated'] );
        $deleted  = count( $this->log['is_deleted'] );
        $affected = $inserted + $updated + $deleted;

        $this->logDescription .= "Inserted: {$inserted}, updated: {$updated}, deleted: {$deleted}";

        $this->wpdb->insert(
            __LOG_TBL__,
            [
                'job'                => $this->job,
                'scope'              => $this->scope,
                'request_time_float' => $_SERVER["REQUEST_TIME_FLOAT"],
                'end_time'           => microtime( true ),
                'affected'           => $affected,
                'description'        => $this->logDescription,
                'log_raw'            => json_encode( $this->log, JSON_PRETTY_PRINT ),
                'logged_gmt'         => $this->system_time['gmt']
            ],
            [ '%s', '%s', '%f', '%f', '%d', '%s', '%s', '%s' ]
        );

        $this->log['message']             = 'success';
        $this->log['message_description'] = $this->logDescription;
        $this->log['affected']            = $affected;

        return $this->log;
    }
}
