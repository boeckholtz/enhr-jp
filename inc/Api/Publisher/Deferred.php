<?php
/**
 * @package: enhr-jp
 */

namespace enhr\Api\Publisher;


use enhr\Helpers\Curly;

final class Deferred extends Curly {

	public function republishApplies() {

		$re = $this->wpdb->get_results( "SELECT * FROM " . __APPLY_TBL__ . " 
		WHERE log_json IS NOT NULL AND status NOT IN ('publish', 'republish', 'error') AND DATE_SUB(logged, INTERVAL 72 HOUR) 
		ORDER BY rand() LIMIT 3" );

		foreach ( $re as $data ) {

			if ( empty( $this->getVacancyID( $data->vacancy_root_id ) ) OR empty( $data->log_json ) ) {

				$this->wpdb->delete(
					__APPLY_TBL__,
					[ 'id' => $data->id ]
				);

				continue;
			}

			$attachments_quantity = (int) $data->attachments_quantity;
			$log                  = json_decode( $data->log_json, true );

			if ( $attachments_quantity < 1 ) {

				$log["exit"]["message"] = __( "No attachments files were uploaded", self::TEXTDOMAIN );

				$this->wpdb->update(
					__APPLY_TBL__,
					[
						'log_json' => json_encode( $log, JSON_UNESCAPED_UNICODE ),
						'status'   => 'error'
					],
					[ 'id' => $data->id ]
				);

				continue;
			}

			if ( $data->enpub_id === null ) {

				if ( ! empty( $log["applied_data"] ) ) {

					$this->endPoint = 'applies';

					$appliedData                         = $log["applied_data"];
					$appliedData['attachments_quantity'] = $attachments_quantity;
					
					//determine if TAS should send emails and add key/value to the API request
                    $appliedData["send_ats_email"] = get_option(__PRFX__.'_send_apply_mails') == '1' ? false : true;
                    $appliedData["secret"] = get_option(__PRFX__.'_rest_api_client_secret_id');

					$this->apply_post_fields = json_encode( $appliedData, JSON_PRETTY_PRINT );
					$setApply                = $this->setApply();
					$apply                   = json_decode( $setApply["content"] );

					$log['enpub_response'] = $setApply["content"];

					if ( isset( $apply->result ) AND $apply->result === 'success' ) {

						$data->enpub_id = $apply->apply_id;

						$log["exit"]["message"] = __( "Vacancy was republished... continue processing attachments.", self::TEXTDOMAIN );
						$log["exit"]["result"]  = "success";

						if ( empty( $log["applied_data"]["vacancy_post_title"] ) OR empty( $log["applied_data"]["company_post_title"] ) ) {

							$info = $this->getExtraInfo( $log["applied_data"]["vacancy_root_id"] );

							if ( is_array( $info ) and count( $info ) === 4 ) {

								$log["applied_data"]["vacancy_post_id"]    = (string) $info["vacancy_post_id"];
								$log["applied_data"]["vacancy_post_title"] = $info["vacancy_post_title"];
								$log["applied_data"]["company_post_id"]    = (string) $info["company_post_id"];
								$log["applied_data"]["company_post_title"] = $info["company_post_title"];
							}
						}

						$this->wpdb->update(
							__APPLY_TBL__,
							[
								'enpub_id' => $data->enpub_id,
								'log_json' => json_encode( $log, JSON_UNESCAPED_UNICODE ),
								'status'   => 'incomplete'
							],
							[ 'id' => $data->id ]
						);
					}
				}
			}

			if ( ! empty( $log["attachments"] ) ) {

				$f = 0;

				foreach ( $log["attachments"] as $i => $attachment ) {

					if ( file_exists( $attachment['path'] ) AND ( ! isset( $attachment['enpub_response'] ) OR $attachment['enpub_response'] != 'success' ) ) {

						$apply_post_fields               = [ 'name' => $attachment['name'] ];
						$apply_post_fields['attachment'] = base64_encode( file_get_contents( $attachment['path'] ) );
						$apply_post_fields['size']       = $attachment['size'];
						$apply_post_fields['apply']      = $data->enpub_id;

						$this->endPoint          = 'apply-attachment';
						$this->apply_post_fields = json_encode( $apply_post_fields, JSON_PRETTY_PRINT );

						$setApplyFinal = $this->setApply();
						$applyFinal    = json_decode( $setApplyFinal["content"] );

						$log['enpub_response_attachments'] = $setApplyFinal["content"];

						if ( isset( $applyFinal->result ) AND $applyFinal->result === 'success' ) {

							$log['attachments'][ $i ]['enpub_response'] = $applyFinal->result;
							$log['attachments'][ $i ]['unlink']         = unlink( $attachment['path'] );

							$this->wpdb->update(
								__APPLY_TBL__,
								[
									'status'   => 'republish',
									'log_json' => json_encode( $log, JSON_UNESCAPED_UNICODE )
								],
								[ 'id' => $data->id ]
							);

							$f ++;

						} else {

							$log['attachments'][ $i ]['enpub_response'] = $applyFinal->result;
							$log['exit']['result']                      = 'incomplete';
							$log['exit']['message']                     = __( "Could not publish the attached file: ", self::TEXTDOMAIN ) . $attachment['name'];

							$this->wpdb->update(
								__APPLY_TBL__,
								[
									'status'   => 'incomplete',
									'log_json' => json_encode( $log, JSON_UNESCAPED_UNICODE )
								],
								[ 'id' => $data->id ]
							);
						}

					} else if ( isset( $attachment['enpub_response'] ) OR $attachment['enpub_response'] == 'success' ) {
						$f ++;
					}
				}
			}

			if ( $attachments_quantity == 2 ) {

				$log['exit']['result']  = 'success';
				$log['exit']['message'] = __( 'The data have been processed successfully', self::TEXTDOMAIN );

				$this->wpdb->update(
					__APPLY_TBL__,
					[
						'log_json' => json_encode( $log, JSON_UNESCAPED_UNICODE ),
						'status'   => 'republish'
					],
					[ 'id' => $data->id ]
				);
			}
		}
	}

	public function removeObsoleteData( $where ) {

		if ( $where === 'applies' ) {
			$dt = \Carbon\Carbon::create( 'UTC' )->subDay( get_option( __PRFX__ . '_obsolete_data_removal_period' ) )->format( 'Y-m-d H:i:s' );
			$this->wpdb->query( "DELETE FROM {$this->wpdb->prefix}" . __PRFX__ . "_logging WHERE logged_gmt < '{$dt}' AND affected = 0;" );
		}
	}

	/**
	 * @param $vacancy_root_id
	 *
	 * @return array
	 */
	private function getExtraInfo( $vacancy_root_id ): array {

		$output = [];

		$output['vacancy_post_id']    = $this->getVacancyID( $vacancy_root_id );
		$output['vacancy_post_title'] = html_entity_decode( get_the_title( $output['vacancy_post_id'] ) );
		$vacancy_post_meta            = get_post_meta( $output['vacancy_post_id'] );
		$output['company_post_id']    = $this->wpdb->get_var( "SELECT post_id FROM {$this->wpdb->prefix}postmeta where meta_key = '" . __PRFX__ . "_company_root_id' AND meta_value = '{$vacancy_post_meta[ __PRFX__ .'_vacancy_company_root_id'][0]}' LIMIT 1;" );
		$output['company_post_title'] = html_entity_decode( get_the_title( $output['company_post_id'] ) );

		return $output;
	}

	private function getVacancyID( $vacancy_root_id ) {
		$args = array(
			'posts_per_page' => 1,
			'post_type'      => 'vacancy',
			'meta_query'     => [
				[
					'key'   => __PRFX__ . '_vacancy_root_id',
					'value' => $vacancy_root_id,
					'type'  => 'INT'
				],
				[
					'key'   => __PRFX__ . '_vacancy_language_code',
					'value' => $this->primaryLanguage
				]
			],
			'fields'         => 'ids'
		);

		$query = new \WP_Query( $args );

		return $query->posts[0] ?? null;
	}
}
