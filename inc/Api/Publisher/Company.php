<?php
/**
 * @package: enhr-jp
 */

namespace enhr\Api\Publisher;


class Company extends Job implements Scopes {

	public function run( $args = [] ) {

		$this->args             = $args;
		$this->saveLogo         = true;
		$this->scope            = 'companies';
		$this->post_type        = 'company';
		$this->scope_taxonomies = [
			'country',
			'branch_primary',
			'branch_secondary'
		];

		$this->preparation();
		$this->fetcher();

		return $this->postAction();
	}

}