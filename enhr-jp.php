<?php
/**
 * @package: enhr-jp
 */
/*
 * Plugin Name: ENHR-JP
 * Description: The plugin will communicate with the ENHRJobPublisher’s API-endpoints
 * Author:      Sedna Software / Vesta Group BV.
 * Version:     2.1.9
 * Text Domain: enhr
 */

//end-header
include (dirname(__FILE__) . '/update.php');

$updater = new ENHR_Updater( __FILE__ , [
    'access_token'  => 'e6ee5757c88bffd62a7abf798410c5ea51cba38a1236b395402ad9ed6dfa78d9',
    'repository_id' => 16904655,
]);

// initialize our updater
$updater->initialize();

// load required files
include (dirname(__file__).'/bootstrap.php');