<?php
$table       = $this->wpdb->prefix . __PRFX__ . '_applies';
$result      = $this->wpdb->get_results( "SELECT * FROM {$table} ORDER by id DESC LIMIT 1", OBJECT );
$appliedData = json_decode( $result[0]->log_json )->applied_data;

$title = __('Beste', TEXTDOMAIN)  . ' ' .  $appliedData->firstName . ' ' . $appliedData->lastName;

$body = '<p style="margin-top: 3rem;">' . __('Bedankt voor je sollicitatie bij', TEXTDOMAIN) . ' $companyMame.</p>
        <p>' . __('U heeft gesolliciteerd naar de functie: $jobTitle', TEXTDOMAIN) . '.</p>
        <p style="margin-top: 2rem;">' . __('Uw sollicitatie wordt zo spoedig mogelijk in behandeling genomen: ', TEXTDOMAIN) . ' $jobTitle.</p>
        <p style="margin-top: 2rem;">' . __('Met vriendelijke groet', TEXTDOMAIN) . ',</p>
        <p style="margin-top: 1.5rem;">$companyMame.</p>';

$message = <<<HEREHTML
<!doctype html>
<html lang="{$this->primaryLanguage}">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700" rel="stylesheet">
</head>
       
    <body>
<div class="message" style="font-family: 'Open Sans', sans-serif; font-size: .9375rem;">

    <div class="message-body">
        {$title}

        {$body}

        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAY1BMVEX///8kp58AopkWpJz4/fzi9PNEsaoMo5us2NSBxsE7rqfy+fkAoJcxrKX7/v7e8fDr9vWc08/V7euRzsrH5uRivLbO6edtwLqn2NRVt7Ci1tJ1w76Iy8a13tu+4uCV0Mxbt7GMVRROAAAP2klEQVR4nO1d2ZaqOhCVBAnIjIoiov7/VzYJqSRMGiah13I/3KFPH2CT1JiqYrf74Ycffvjhhx9++OGHH3744YelYYbpqbjGz+MzvhanNIzWfqBZkdj5wcCEIMKAyn9jw7fOxyJ113626XCLS8kMGx3AJVcPZ89HFKz9lKNhPi6km53Kk3j+Jf6Xq7k/+qhJD5coNyr9V52mZ7yO6f8SzuSCSY1EuSUN65zfnyWOTn62DOQhZQOXzI0sTtZ+bl2kmbo7MSIHp0ha4halxfPsl/KIFZb54x+IZZp5WKFnXB7vpMxN7YtlENi35es4F9uWyvBCVH7nt/QA+8c9w0SQxOdis0LpOgo/QpxQ+28GUZEbCCsrv8ntejWIwu84dCGC9GggIkje9d/Pl5AckOSHj+a4ixwPYiU9yx53kWXg3iU/jJ0JgpQ4Pux1ggds9IXx8OUG9c4TH8ul7pBUVluQyCBXNqjxmOGK0dPnRgejg706x5PUMJg4cz3OQzgOyIhXtZGB48kFPKQzXjnJwfkrNfN6HENL2aGzLSDH/g7bgxjPldyAqwwU8CwS2ED0FBz9eAXjEVykCkXZMhvJfYIbgPxikTu8QajYCPJc7DbmHTaKN6ucf0Yh+WHjtOSdolzo1csXxfGuLGC29H2TzIN3eV34VgD3pYjg5Qs2uQCVg7L98ncr9bgv4yRvORFUYTrcKcfktvzdTkoeBn9Nw4nwBb2WloqrqmO+qN6CI9c4i9heBUclUvK/IhQCKUgHOi54l1xZwcO3/cXgzF8vWUy9BWfFSrxWCGxu4MZZywijef6ylWjjZHBh9Jeg6GYKwXyBG+ggspajGFhKtmItguVG4u8Z+3OrgcCSdh45M198EC6VvsHWvBGVqxAk91kvPRg8bsPZnKrAVF3RlQnudlzjkTntoqJFvVW3aIVDtaHQfN7NRXoyq2lRFRHIy1wKNd8YwdKD46JozXO5p+LJXOa55GTwZ0KzBFOFkjM8r56D5hDGawbvP1GcbWsrBMvH4lbxPPlKkXKwe9jSkZfD9+nUPJjiyizj7Y6GyR/Mn3gdJSA0NnOgV+E6i7K5KVrmuxnZzwgO/MmmyI6iZbyvp9U/wkaTnTfXl5Y+nu/J5kJg8Ycbrx/OQsuQbbgyDdhTPXBp6ucNVGaD6U9bRMUSTtgHiyImk0QoEwzRxuyEgAmn7KO22FXsUWTP/WSzIa9WgYw5lJJ7lGwg5O1D4o13bIQe3ZK73QYXpRHe6Unq0a0KIQM3GCNCDGHr0fZ8GRXu2HWIYQlniL+WBeiagVYf3kyJbRcmS3EaGLseweP2lj2NnAHg15BBugaSdQbeSuLpDfg2xYM8Z7GEW/XWVAitP0CeTKFHv1W/MgkjlL6IKQ7LPdaMgG06QOuDyz3jscCSeEAdirY2jaDIaqaU+dIIoH5Be5vyoMvwFq3ImxHnoduUb1KcLflUcwLiPKypTcXJ1Xajwgb23jC9kY6wL+sCEqe6Rp9X5WzfnQlE3Opw5X/Qi2S5edlw6oIhzS0r57oQKpaRXisqKJpNB767u5cXRc5rCvbQLKWVc3MPg1Z8JcQoKbELPRYWQvJbz15EQ355LURlVEcVaHDy2BlwDkklHe0YLlCrMjtswzS9JCjNQ1XcDp400jkfC9FgT/37iLOdSV5nP9plrHwpAUHUKTvn5vC7p4VRkqbJAPt7s0qGd+O221mMU2AMkK0H36XNGoc9m4ggYfiZM08HJO0cZfDvdW1vHnADfnZnfkvihYGXxjiIvGolIBzCGk/EtzRuhiL72qgE9isYzdA5d7JkEz9Gmbp1TKVaUNzTM24li1dm5qGbpxl3no8DLCIIbfPn+9bd2FJPbAkyc692XawWdppW1y0NZO13kW8lbpRkkGZ5QHm0hp/CGbYOAroZTjwcjqzWzlD64HoYGtjY71zHQwjl8Lt7CC80XNPKA2pHv30MpyRz3EPHRWVlbB/Dqj7YTEJFkiBc0Diiqda7HRz2MjS80YNJsu6dDwqxl2GHzoRf9T6rmqrpp30JwZBwSP0wNtuhlLJgdWSPZzcYIgHxEE3xh/BCw+ZX9rAdOwHD7BYzPO+iC9gbZztdIYPEd57P3JeEzRpDPwo50jtYhebzQbOShqcSvmeIFV0HQcuwdLOAKOokV8bIhH4RKHMChmpScw+L3jBTkHDDn49zK7unxVC0seHh9HbyAE/asBMcy1f6ooshvNZmmkwo08/ZpSq26JXD+jviWS5SxpKBa5pmJeZmlKb7+jt2yx8lUc2LABdCPYMHyayqmzsZ8txh8+Q+gPX/rEyrOihNhrGsgLQN3/czmwbfPtUJRi6k07TPVFl46OAoEgv5IPUQHTIulVfcyZAn8ZvBrihy+1xSW0WTvdaiztBWGNL/xOgpGpMxyas1exzkQCh0Fg/AJQfXehsc9Vi+myHpZCjqDshn2/XC+gxvDYbUcTQECCtIvdfcFiz0PBRPFl0XLHoZ9kV3YC7I55RiFS+3DmXeymEiGKpgUXTuNX4KOgJq0WumBiKbWy/DC1/lZhoJNLNGrqb61T6/tMYQijOp6m4zZHX9tybB8rerjcrDAa/2qBCdxn0MgYjfjH1sfXPBHVMNhiHX98ypaTFkP90jowWuxe5dixFWL6SK1YXFD5MK6Q0aBNutV8JyfQ6Cq4Vpx4dwmLwX/oUBW9+uMSRe5V1RIRQpIvghI8X2qdPJUM0SCa8NHEU5qg+3NGYIJ2afDeK++xItv1RxTAOVIb4l+1OGMNVpcAaCjTjZp1AzXvlAnGH9RqFa9tvveXfkYyK4v0ZRBtt7rTxNf2xRqTXBsIpJY/bDEzTvVPYf4lRWSjiBYdc6ieoKjY4CFtO0znF6GfJGITCNtSCb6wUMr4uz8qJJDFFXpZ2s2P7MkCm5Vj1jH0OSmSpDXLs+F0OhDHkpIRPE0Qy9znFGkKtnLuQHsL3UUro9DD3ohOIM6925lb2U6s1UzLWDOx5Ig6HVHauJsnaNcl+m4fFLgyFWRv90MnxVDEV0xfOabIPcOxl2WQvj4FMAw+6nDkR9hUbKAXVdCBhijwPhw6WQ+6WLIb+r3A9B9ZjMZWnawzB+JtCv1WXxQZX05YUEQ43sX3XhhsCCPbyA+Q3rAdI7hsI8qwwbXtsDIeIduTve6bXd+9yZCsL11jjrZm+3KbCdfqkmw8415E42rxHhqQW+FTs9b6jL7ml0GsKQpUybvziG4Vs5LFSlIqwZJ/HoYCjCh+5GJ8FQ40yJqZrm8doohk1dGijBEa+4q6x3VvdfuyNgKGXqPvl7DWDIvJpmpmYUw6Y9hOCOLlEI2TGqGfZZzXHvyWKAl4u6TkteA3ZpFYM1hjKMYsiVg6gJAO3C/l/woXSCWhzZk4mCSKVzqMMQXVopgbp3Mo4hZCr4No0ghmNGBsQK+8yC2Uo77rGbIYS/zWejkBZf+xy4ka8bxVDUV1mnIHAha8sFQMaO6GWfToXiwZCeLAbs7I6nEF6bVgsT82Ib6YBRDIWCw4iIUcjCq5cZHPo1gZogslFQHTG+WMQWDeF5a/il/EINVTOOYdIV40No0Hn0BM95drsYirxJK80uu5i0KrdZ0sOvufDjGEJPeW19hKCE/QxL6QyDjjwNaMzWXgxFvlyLIbt1XSfzFHUvQzhZbSryrEnRU/zKBLc4ip+gToawKVrHDqKgXbNNi167EQQb7xny0K/FUIwg46iPBYwOjW1MzpnMwV4a5pTC4X+heWoJxzqtoKgHVJPjuutge28Z8sXqmN70xIpGOTSsVRAbSsIH+Vc+XZrlClLSkbu9VK+ymckQtkfzIIyteSOAsn0aNPVewHRI+cdddbr7u49YpgxZ17ZLGV2z6vTTIy/b5Tfyq2TI6UBv2aiasA9V+FZ/V2DwdUdHmvS1t06MoxJvyksC+ufdfxIW16v96NMB7qn84yKV7E1xZ7frmuyH9ScJhPjqHtdSQzZ5qMYXIc2S7jgXqhoXnA88O6DBQL8LhkYq/6daX9ZiaBxbAJjk/ptidijXH9LnQ+3L/xFEUfWA9IsJqec/YMnXheh5HVTaTLfpphvVFTybYYsWqHbSi0RWh5LHGrIkNNr8Hy2WMtAcWJ1ebtNtV+wDlOLeYV29NOmg2w62KsSAi6ENBjRq/g9tpA+RpRs8lraMp7ff3yX7skfMcKEHiQMs6EoQE+XwiMZs9A+2qRzCotm5VoOz/W0qj+9HDQqiQVersGZbkFMPx02/trbeACX1qNaBTBtXtO0gMUKT9uiucvi23Ewq2xl6jr4/I8db7kSUM1jG6NEKdObypibPqpDfg5nyTQ8Lb3Z6hIyZJukKm2zWJCq9e1MGBdHqCbLJAMORenSa31VK8ybjYBvNIYQU1ORMHbe8ABI8ixAylAZjezPNFC0z2hIKhGh704bUKeMzfJb9gjc36Vp+bmMWt7m0+hvLSMkPh83kcL3wtubsyum4c32Mgi7ihqZdPwTB+fRD6cK3ZiyshlQqmfk+ZZB421lEaJOlmFF0ss1IomuIJZz17C/cSqzvyi+Ezpxeccgm8oquDOq9mZ1l6iWtPznKlCVkaPYqihtZ/XNrao3cEs9SeoLaxTgLQfpqi4yJZ3XDaw7dVb6gudDXNmJv1QH0yjdQF/tw2IWs83lVhuC19AruqrFN5LXU1d8jyJZfwV1Vl01W+R6L8gXNZb94E5UexVKf4n17X0lw6U+gUoq8AeSLUD7mvvwXQtlwLvyFL7grUMrd5/dk2gjoh1xJ9sWd+pDVMl9K3NIaMvy9c1NlXNbXfP8TFQt0/k68KHP32Pie0xjRhgCMneWPM1zZeYn9r4bgBW2EIMbSYpEoStT68vGQeak42kvaf1sRwRXSRCc21BH516XebaB+hXiVbxMGNhuSR/zjIgKSyLGYeOII0fEwY9bRRPB5/ieIpZlfw08UMKuuLYz856xPsbfWFUEVZuyz6UjEy4q5SAYxknltY/2aLLPI2ANhhM/2HCRTZTAtybZxmpBeSDU+C+FXPDHyiC7KJNNveNqaiG5GtbMwIX4+fr+aR6Vvlhy+Hae9R5pzkiVLz3DscLgz4B6VnnW8wXIz93EWLa8YIeP8PEUDygjCO1G6okm2jaOgJqLiguWkS4KwdTk+Eo1NG9kvdeaAMp5pe3AfjlHbbAQZ1vlepGEf0ej0zJR+Z7oBps96XxhJbBGkPDKdUI9IqUSs18V53mzbLoqi/OftmJ8PWJkWWL2T/B980I52L98tg9QevaKKaVt3BUK/RtD4BYLzbZhALexP95dfLuab4RAN/sS4/yN+Fdzw6pRbtr2abRCU2VuXvz4EYfE8G3TWf3tbwup5fvzvlq+F/cm+Xyyf2RE6kYYPJS2X18p7xxH8R7j7MEkfhX27xfHtWpzS8L/uzR9++OGHH3744Ycffvjhhx/+Ef4AUsmv9MV7xKIAAAAASUVORK5CYII="
             style="margin: 2rem 0; width: 100%; max-width: 150px">
    </div>
    <div class="address" style="line-height: 1.5">
        RDC | De Boelelaan 7<br>1083HJ AMSTERDAM<br>
        06 57 88 14 71<br>
        <a href="mailto:recruitmenat@drc.nl">recruitmenat@drc.nl</a>
    </div>
</div>
    </body>
</html>
HEREHTML;
