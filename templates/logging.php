<?php
/**
 * @package: enhr-jp
 */
?>
    <form id="logWrapperForm" class="cpt">
        <input type="hidden" name="action" value="jqxhr">
        <input type="hidden" name="scope" value="log">
        <input id="showMoreResult" type="hidden" class="form-field" name="log[more]" value="1">
        <input id="showMoreInitial" type="hidden" class="form-field" name="log[initial]" value="0">
        <nav id="logNav">
            <label class="mr-1">Row to display
                <select id="row2Display" class="form-field" name="log[limit]">
                    <option>2</option>
                    <option selected>10</option>
                    <option>25</option>
                    <option>50</option>
                    <option>100</option>
                </select>
            </label>
            <input id="clearBtn" type="button" value="Clear Result">
        </nav>
        <table id="logTbl">

            <col width="100">
            <col width="100">
            <col width="100">
            <col width="100">
            <col class="col-md">
            <col width="120">
            <col width="120">

            <thead>
            <tr>
                <td>
                    <select id="showJob" class="form-field" name="log[where][job]">
                        <option selected disabled>Updated</option>
                        <option value="manual">Manually</option>
                        <option value="cron">Scheduled</option>
                    </select>
                </td>
                <td>
                    <select id="showScope" class="form-field" name="log[where][scope]">
                        <option selected disabled>PostType</option>
                        <option value="vacancies">Vacancies</option>
                        <option value="companies">Companies</option>
                    </select>
                </td>
                <td>Execution</td>
                <td>
                    <label for="skipEmpty">Affected
                        <input id="skipEmpty" class="form-field" name="log[where][affected]" type="checkbox" value="0" checked></label>
                </td>
                <td class="col-md">Description</td>
                <td>Requested Period</td>
                <td>Performed (UTC)</td>
            </tr>
            </thead>
            <tbody></tbody>
        </table>
        <nav id="paginatorNav">
            <span id="paginator"></span>
        </nav>
        <input id="showMoreBtn" type="button" value="Show Result">
    </form>
<?php

\enhr\Helpers\Kit::stats_javascript( __ENHR_DEBUG__ );

