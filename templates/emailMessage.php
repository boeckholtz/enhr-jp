<?php

$result      = $this->wpdb->get_results( "SELECT * FROM " . $this->wpdb->prefix . __PRFX__ . '_applies' . " ORDER by id DESC LIMIT 1", OBJECT );
$appliedData = json_decode( $result[0]->log_json )->applied_data;

$title               = __( 'Beste', TEXTDOMAIN ) . ' ' . $appliedData->firstName . ' ' . $appliedData->lastName;
$contact_information = trim( strip_tags( $this->companyData[ __PRFX__ . '_vacancy_application_contact_information' ][0] ) );

if ( empty( $contact_information ) OR
     preg_match( '/Online/mi', trim( strip_tags( $contact_information ) ) ) OR
     strlen( $contact_information ) > 72 ) {

	$contact_information = $this->companyData['company_title'];
}


$body = '<p class="header">' . __( 'Bedankt voor je sollicitatie bij', TEXTDOMAIN ) . ' <strong>' . $this->companyData['company_title'] . '</strong>.</p>
        <p class="text">' . __( 'U heeft gesolliciteerd naar de functie: ', TEXTDOMAIN ) . '<strong>' . $this->vacancyData['title'] . '</strong>.</p>
        <p class="text">' . __( 'Uw sollicitatie wordt zo spoedig mogelijk in behandeling genomen', TEXTDOMAIN ) . '.</p>
        <p class="text">' . __( 'Met vriendelijke groet', TEXTDOMAIN ) . ',</p>
        <p class="footer">' . $contact_information . '.</p>';

if ( ! empty( $this->companyData['thumbnail_path'] ) AND file_exists( $this->companyData['thumbnail_path'] ) ) {

	$companyLogoMime = wp_check_filetype( $this->companyData['thumbnail_path'] );

	$companyLogo = '<img id="logo" src="data:' . $companyLogoMime['type'] . ';base64,' . base64_encode( file_get_contents( $this->companyData['thumbnail_path'] ) ) . '">';

} else {
	$companyLogo = null;
}

$address = '';

if ( ! empty( $this->companyData['meta'][ __PRFX__ . '_company_visit_city' ][0] ) AND
     ! empty( $this->companyData['meta'][ __PRFX__ . '_company_visit_number' ][0] ) AND
     ! empty( $this->companyData['meta'][ __PRFX__ . '_company_visit_postal_code' ][0] ) AND
     ! empty( $this->companyData['meta'][ __PRFX__ . '_company_visit_street' ][0] ) ) {

	$address = $this->companyData['meta'][ __PRFX__ . '_company_visit_street' ][0] . ' ' . $this->companyData['meta'][ __PRFX__ . '_company_visit_number' ][0] . '<br>' . PHP_EOL;
	$address .= strtoupper( preg_replace( '/[^a-zA-Z0-9]/', '', $this->companyData['meta'][ __PRFX__ . '_company_visit_postal_code' ][0] ) . ' ' . $this->companyData['meta'][ __PRFX__ . '_company_visit_city' ][0] ) . '<br>' . PHP_EOL;
}

if ( ! empty( $this->companyData['meta'][ __PRFX__ . '_company_email' ][0] ) ) {
	if ( $address !== '' ) {
		$address .= PHP_EOL . '<br>';
	}
	$address .= '<a href="mailto:' . $this->companyData['meta'][ __PRFX__ . '_company_email' ][0] . '">' . $this->companyData['meta'][ __PRFX__ . '_company_email' ][0] . '</a>';
}

if ( $address !== '' ) {
	$address = '<div id="address">' . $address . '</div>';
}

$this->emailMessage = <<<HEREHTML
<!doctype html>
<html lang="{$this->primaryLanguage}">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
</head>
    <body>
    <style>

	    @import url('https://fonts.googleapis.com/css?family=Open+Sans:400,700');
	    
	    .message{
	    font-family: 'Open Sans', sans-serif !important;
	    font-size: .9375rem !important;
	    }
	    
	    #message-body p.header, #message-body{
	        margin-top: 2rem;
	    }
	    
	    /*#message-body p.text{
	        margin-top: 1.5rem;
	    }*/

		#logo{
			margin: 1rem 0;
			width: 100%;
			max-width: 150px
		}
		
	    #address{
		    line-height: 1.5;
		    margin-top: 1rem;
	    }

</style>
<div class="message">

    <div id="message-body">
        {$title},
        {$body}
        {$companyLogo}
    </div>
    {$address}
</div>
    </body>
</html>
HEREHTML;
