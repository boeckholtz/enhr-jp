<?php
/**
 * @package: enhr-jp
 */

use enhr\Helpers\Curly;
use enhr\Helpers\Log;
use enhr\Helpers\MultiLingual;

$plugin_url = $this->plugin_url;
?>
    <style>


    </style>
    <div class="wrap dr--setting-wrap">

        <div id="enpubliherHeader"><img src="<?= $plugin_url . 'assets/publisher_logo_white.svg' ?>"
                                        style="width: 100%; max-width: 480px;"></div>

		<?php settings_errors();

		global $wpdb;

		?>

        <form method="post" action="options.php">
			<?php

			echo '<listing style="margin-bottom: 12px; padding: 12px; white-space: pre-wrap; background: #42476E !important; font-family: Consolas !important; border: 1px gold dotted !important; color: gold !important; font-size: 15px; padding: 4px 12px !important; border-radius: 4px !important;">';
			//var_dump( get_option( '__YODMP__' ) );
			echo '</listing>';

			settings_fields( __PRFX__ . '_general_settings' );
			do_settings_sections( __SLG__ );
			submit_button();
			?>
        </form>
    </div>
<?php

add_action( 'admin_print_footer_scripts', function () use ( $plugin_url ) {
	?>
    <script>

      jQuery(document).ready(function ($) {

        let publisherManagerWrapper = $('#publisherManagerWrapper'),
          syncResponse = $('#syncResponse');

        publisherManagerWrapper.on('click', '#enhrSyncBtn', function () {


          syncResponse.html('<img src="<?= $plugin_url . 'assets/loadingcircle_ver1.gif' ?>" style="width: 100%; max-width: 200px;">');

          $('#enhrSyncBtn').attr('disabled', 'disabled');

          let data = {
            action: 'jqxhr',
            sync: {
              job: 'manual',
              scope: $('#postTypeSelector').val(),
              since: $('option:selected', $('#periodSelector')).val()
            }
          };

          $.post(ajaxurl, data, function (response) {

            syncResponse.html('');

            $.each(response.job, function () {

              //let text = (this.message !== 'error' ) ? this.scope : this.message_description;

              syncResponse.append('<div class="' + this.message + 'Response">' + (this.scope !== undefined ? this.scope + ' - ' : '') + this.message_description + '</div>')

            });

            $('#dump_response').show().html(response);

            //console.log(response.job);

          }, "json").always(function () {
            $('#enhrSyncBtn').removeAttr('disabled');
          });
        });

        $('form').on('click', '#locker', function () {

          let inputs = $('tr.api-data input');

          if ($(this).attr('state') !== '') {
            inputs.attr("readonly", true);
            $(this).attr('state', '');
          } else {
            inputs.removeAttr("readonly", true);
            $(this).attr('state', 'un');
          }

          $(this).toggleClass('unlock-icon lock-icon');
        });

      });
    </script>
	<?php
}, 99 );
