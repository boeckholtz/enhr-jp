<?php
/**
 * @package: enhr-jp
 */

$table = __APPLY_TBL__;
$dates = $output = [];

$dt = \Carbon\Carbon::now( 'UTC' );

$dates['today']     = $dt->copy()->today()->format( 'Y-m-d' );
$dates['week']      = $dt->copy()->subWeek( 1 )->format( 'Y-m-d' );
$dates['month']     = $dt->copy()->subMonth( 1 )->format( 'Y-m-d' );
$dates['quarter']   = $dt->copy()->startOfQuarter()->format( 'Y-m-d' );
$dates['year']      = $dt->copy()->firstOfYear()->format( 'Y-m-d' );

$queryUnionAll = [];

foreach ( $dates as $period => $date ) {
	$queryUnionAll[ $period ] = "SELECT '{$period}' as period, status, COUNT(*) as statusCount, (COUNT(*) / _statusCount ) * 100 AS percent FROM " . __APPLY_TBL__ . ", (SELECT COUNT(*) AS _statusCount FROM {$table} WHERE status IN ('publish', 'republish') AND DATE(logged) >= '{$date}' ) AS aplliesTotal WHERE status IN ('publish', 'republish') AND DATE(logged) >= '{$date}' GROUP BY period, status";
}

$query  = implode( PHP_EOL . ' UNION ALL ' . PHP_EOL, $queryUnionAll );
$result = $this->wpdb->get_results( $query, OBJECT );

foreach ( $result as $i => $row ) {

	$output[ $row->period ]['count'][]   = (int) $row->statusCount;
	$output[ $row->period ]['percent'][] = (float) $row->percent;
}

?>
    <div id="appliesWrapper">

<?php

$dutchPeriods = [
	'today'     => 'Vandaag',
	'week'      => 'Week',
	'month'     => 'Maand',
	'quarter'   => 'Kwartaal',
	'year'      => 'Jaar'
];

foreach ( $output as $period => $values ) {

	//\enhr\Helpers\Log::dmp(array_sum($values["percent"]));

	echo "<div class=\"statistics-box\">" . __( $dutchPeriods[ $period ], \enhr\Base\BaseController::TEXTDOMAIN ) . "<p>" . array_sum( $values["count"] ) . "</p></div>";
}
?>
    </div>

<div id="a-container">
    <div class="a-column">
    <h1>Top Applies for the Vacancies for the Last Month</h1>
        <?php
        $top = $this->wpdb->get_results( "SELECT logged, status, log_json as log, COUNT(vacancy_root_id) AS most_frequent FROM " . __APPLY_TBL__ . " WHERE status IN ('publish', 'republish') AND DATE(logged) >= '{$dates['month']}' GROUP BY vacancy_root_id ORDER BY most_frequent DESC, logged DESC limit 10" );

        echo \enhr\Base\Apply::renderRows( $top );
        ?>
    </div>
    <div class="a-column">
        <h1>Top Applies for the Vacancies for All Times</h1>
	    <?php
	    $top = $this->wpdb->get_results( "SELECT logged, status, log_json as log, COUNT(vacancy_root_id) AS most_frequent FROM " . __APPLY_TBL__ . " WHERE status IN ('publish', 'republish') GROUP BY vacancy_root_id ORDER BY most_frequent DESC, logged DESC limit 10" );

	    echo \enhr\Base\Apply::renderRows( $top );
	    ?>

    </div>
</div>
