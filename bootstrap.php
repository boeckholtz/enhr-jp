<?php

globaL $wpdb;

define( '__ENHR_DEBUG__', true );
define( '__PLGNM__', 'enhr-jp' ); /* name */
define( '__PRFX__', 'enhr' );     /* prefix */
define( '__SLG__', 'enhr_jp' );   /* slug */
define( '__LOG_TBL__', $wpdb->get_blog_prefix() . __PRFX__ . '_logging' );   /* Logging table */
define( '__APPLY_TBL__', $wpdb->get_blog_prefix() . __PRFX__ . '_applies' );   /* Logging table */
define( '__DIR_JP_APPLIES_ATTACHMENTS__', wp_upload_dir()['basedir'] . '/' );   /* Logging table */
define( '__DIR_JP_MEDIA_UPLOAD__', wp_upload_dir()['basedir'] . '/enhrpublisher/' );   /* Logging table */
/**
 * The code that triggers during plugin activation
 */
register_activation_hook( __FILE__, function () {

    add_action( 'activated_plugin', 'my_save_error' );

    function my_save_error() {
        file_put_contents( wp_upload_dir()['basedir'] . '/error_activation.txt', ob_get_contents() );
    }

    enhr\Base\Activate::activate();
} );

if ( file_exists( ( dirname( __FILE__ ) . '/vendor/autoload.php' ) ) ) {
    require_once dirname( __FILE__ ) . '/vendor/autoload.php';
}

/**
 * The code that triggers during plugin deactivation
 */
register_deactivation_hook( __FILE__, function () {
    enhr\Base\Deactivate::deactivate();
} );

add_action( 'plugins_loaded', function () {
    load_plugin_textdomain( __PRFX__ );
} );

/**
 * Starting the magic itself
 */
if ( class_exists( __PRFX__ . '\\Init' ) ) {
    enhr\Init::register_services();
}

/**
 * @description Load neccessary js files
 */
add_action( 'wp_enqueue_scripts', function() {
    /**
     * @description Check if description is loaded
     */
    if ( ! wp_script_is('jquery', 'enqueued'))
        wp_enqueue_script('jquery');

    wp_enqueue_script('enhr-apply-js', '/wp-content/plugins/enhr-jp/assets/enhr-apply.js');
} );