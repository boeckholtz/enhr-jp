jQuery(document).ready(function ($) {
  let gw = $('#acf-gradient-repeater-wrapper')
  let i = $('#company-gradient-color-initial input:hidden')
  let f = $('#company-gradient-color-final input:hidden')

  function toGarident () {
    gw.find('preview').css('background-image', 'linear-gradient(to right, ' + i.val() + ', ' + f.val() + ')')
  }

  gw.append('<preview><title>Company Name</title></preview>')
  toGarident()

  $('#company-gradient-color-initial input:hidden, #company-gradient-color-final input:hidden').on('change', function () {
    toGarident()
  })

  $('#publishing-action input[type=submit]#publish').on('mouseenter', function () {

    let cls = $('#company-layout-selector select option:selected')
    
    if (cls.val() === 'featured' && $('#set-post-thumbnail img').attr('src') === undefined) {
      alert('Select feature image')
    }

    if (cls.val() === 'gradient' && (i.val().length + f.val().length) !== 14) {
      alert('Set all gradient colours')
    }
  })
})
