jQuery(document).ready(function ($) {
  $('ul.nav-tabs > li').each(function () {
    $(this).on('click', function (event) {
      $('ul.nav-tabs li.active, .tab-pane.active').removeClass('active')
      let anchor = $(this).find('a').attr('href')
      event.preventDefault()
      $(this).addClass('active')
      $(anchor).addClass('active')
    })
  })
})
