jQuery(document).ready(function($) {

  /**
   * Removes annoying wordpress core/plugin update info
   */
  if ($('.update-nag').length > 0) {
    $('.update-nag').remove();
  }

  let logWrapperForm = $('#logWrapperForm');

  let clearFunc = function() {
    $('tbody.page').remove();
    $('#showMoreBtn').val('Show Results').show();
    $('#paginator').hide();
  };

  logWrapperForm.on('change', '.form-field:visible', clearFunc).
      on('click', '#clearBtn', clearFunc);

  logWrapperForm.on('click', '#paginator li.key:not(.activeBullet)',
      function() {

        let page = $(this);

        $('tbody.page').hide();
        $('#logTbl').find('tbody.page').eq(page.val()).fadeIn();
        $('.activeBullet').removeClass('activeBullet');
        $(this).addClass('activeBullet');
      });

    logWrapperForm.on('click', '#showMoreBtn', function(e) {

      if (e.ctrlKey) {
        clearFunc();
      }

      $('#showMoreBtn, #clearBtn').attr('disabled', 'disabled');

      $('#showMoreInitial').val($('.logRow').length);

      $.post(enhrAjaxUrl, $('#logWrapperForm').serialize(),
          function(response) {

            if (response.message === 'success') {

              if (response.more === undefined) {
                $('#showMoreBtn').hide();
              }
              else {
                $('#showMoreBtn').val('Show More Results');
              }

              $('tbody.page, #paginator').hide();
              $('#logTbl').
                  append('<tbody class="page">' + response.body.join() +
                      '</tbody>');

              let pages = $('tbody.page').length;

              if (pages > 0) {

                let paginator = '<ul id="paginator">';
                for (let i = 0; i < pages; i++) {
                  paginator += '<li class="key" value="' + i + '">' + (i + 1) +
                      '</li>';
                }
                paginator += '</ul>';

                $('#paginator').replaceWith(paginator).show();

                $('#paginator').find('li.key:last').addClass('activeBullet');

              }

            }
            else if (response.message === 'error') {
              $('#paginator').html(response.body);
            }
            else {
              //console.log(response);
            }

            console.log(response);

            $('#showMoreBtn, #clearBtn').removeAttr('disabled');

          }, 'json').fail(function(jqXHR) {

        $('#paginator').
            html('HTTP response error: ' + jqXHR.status).
            addClass('errorResponse');

        $('#showMoreBtn, #clearBtn').removeAttr('disabled');
      });

    });

});