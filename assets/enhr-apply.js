jQuery(document).ready(function($) {
  
  let $form = $('#apply-form');
  /**
   *
   * @var requiredFieldName
   * If you don't want to have the required attribute just simply replace it with .required
   */
  let requiredFieldName = '.required';
  let $requiredFields = $(requiredFieldName);
  let $maySubmit = false;
  
  /**
   * $inputs
   */
  let $email = $('#apply-form [type=email]'+requiredFieldName);
  let $phone = $('#apply-form [type=number]'+requiredFieldName);
  let $file = $('#apply-form [type=file]'+requiredFieldName);
  let $fileExtensions = ['jpg', 'jpeg', 'pdf', 'doc', 'docx'];
  
  let $gender = $('#apply-form [type=radio]'+requiredFieldName);
  let $button = $('#apply-form [type="submit"]');
  
  // ok we may submit but we want to validate some more stuff
  $form.on('submit', function(e) {
    $('.error-message').remove();
    e.preventDefault();
    
    $button.disable();
    $requiredFields.each(function(i,o) {
      if ($(o).val() == '') {
        $(this).addError();
        $maySubmit = false;
      }
      else
        $(this).removeError();
    });
    
    // check for gender
    if ( ! $gender.is(':checked')) {
      $maySubmit = false;
      $gender.addError();
    } else {
      $gender.removeError();
    }
    
    // check email field.
    if ($email.val() != '') {
      $maySubmit = validateEmail($email.val());
      if ( ! $maySubmit)
        $email.addError();
      else
        $email.removeError();
    }
    
    if ($phone.val() != '') {
      $maySubmit = $.isNumeric($phone.val());
      if ( ! $maySubmit)
        $phone.addError();
      else
        $phone.removeError();
      
      if ($phone.val().length < 9) {
        $maySubmit = false;
        $phone.addError('minimal-length');
      }
      else
        $phone.removeError();
    }
    
    // $file.removeError();
    if ($file.files == 'undefined') {
      let $validate = (validateFile($file, $file.data('max-filesize')));
      if ( ! $validate) {
        $maySubmit = false;
        $file.addError('error-filesize');
      } else if ($validate < 0){
        $maySubmit = false;
        $file.addError('error-extension');
      } else {
        $file.removeError();
      }
    }
    
    let $formData = new FormData($('#apply-form')[0]);
    let $redirectToPage = '/bedankt/?i=' + job_id;
    
    if ($maySubmit) {
      $.ajax({
        type: 'POST',
        enctype: 'multipart/form-data',
        url: ajaxurl,
        data: $formData,
        cache: false,
        processData: false,
        contentType: false,
        dataType: 'json'
      }).done(function(data) {
        // enable button
        $button.enable();
        if (data.result === 'success') {
          window.location.href = redirectToPage;
        } else if (data.result === 'error') {
          $('.generic-error').html($('.generic-error').data('error'));
        } else {
          $('.generic-error').html($('.generic-error').data('broken'));
        }
      });
    } else {
      $button.enable();
    }
    return false;
    
  });
  
  $file.on('change', function() {
    $(this).removeError();
    let $validate = (validateFile($(this), $(this).data('max-filesize')));
    if ( ! $validate) {
      $(this).addError('error-filesize');
    } else if ($validate < 0){
      $(this).addError('error-extension');
    } else {
      $(this).removeError();
    }
  });
  
  /**
   *
   * @param email
   * @returns {boolean}
   */
  function validateEmail(email) {
    let emailExp = new RegExp(/^\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i);
    return emailExp.test(email);
  }
  
  /**
   *
   * @param input
   * @param maxFileSize
   * @return {boolean}
   */
  function validateFile(input, maxFileSize = 8388608) {
    let fileExtension = (input[0].files[0].name.split('.').pop());
    
    if ( ! $fileExtensions.includes(fileExtension)) {
      return -1;
    }
    
    let fileSize = input[0].files[0].size;
    // if its above 8mb
    if (fileSize >= maxFileSize)
      return false;
    
    return true;
  }
  
});

jQuery.fn.extend({
  addError: function(sAttr = 'error') {
    this.parent().append('<div class="error-message">'+this.data(sAttr)+'</div>');
  },
  removeError: function() {
    this.parent().find('.error-message').remove();
  },
  disable: function() {
    this.attr('disabled', 'disabled')
      .addClass('disabled');
  },
  enable: function() {
    this.removeAttr('disabled')
      .removeClass('disabled');
  }
});