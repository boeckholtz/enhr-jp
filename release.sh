#!/bin/bash

content=$(cat enhr-jp.php)

version=$(echo "$content" | sed -nre 's/^[^0-9]*(([0-9]+\.)*[0-9]+).*/\1/p' | sed -n '1p')
echo Current version number: "$version"
echo Enter new version number:

read version_number

php header/write-header.php ${version_number}

git config --global push.followTags true
git add .
git commit -m "new release $version_number"
git tag -a $version_number -m "Releasing version $version_number"
git push --follow-tags

curl -X POST -H "Private-Token: zHnE2dzVsHfMJ7ssj7Hq" -H "Content-Type: application/json" \
   -d '{"description":"Releasing version '$version_number'"}' \
   https://gitlab.com/api/v4/projects/16904655/repository/tags/${version_number}/release