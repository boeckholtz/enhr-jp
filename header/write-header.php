<?php

$replaceable = '<?php
/**
 * @package: enhr-jp
 */
/*
 * Plugin Name: ENHR-JP
 * Description: The plugin will communicate with the ENHRJobPublisher’s API-endpoints
 * Author:      Sedna Software / Vesta Group BV.
 * Version:     '.$argv[1].'
 * Text Domain: enhr
 */

//end-header';

$content = file_get_contents('enhr-jp.php');
$content = explode('//end-header', $content);
$content = $content[1];

$f = fopen('enhr-jp.php','w');
fwrite($f,$replaceable.$content);
fclose($f);